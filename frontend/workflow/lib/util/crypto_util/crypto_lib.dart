import 'dart:convert';
import 'dart:math';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import "package:pointycastle/export.dart";
import "package:pointycastle/pointycastle.dart";
import 'package:workflow/util/cache/cache_lib.dart';
import 'package:workflow/util/crypto_util/crypto_util.dart';
import 'package:x25519/x25519.dart' as x22519;

import '../data_util/data_util.dart';

//以下算法以方便传输为目标，所有传入的数据如果是二进制则需要bufferToHexStr，
enum CryptoAlgorithm { ECC, X25519, SM2, ECDH }

enum HashAlgorithm { MD5, SHA256, SHA512, RIPEMD160, Keccak256, SM3 }

class CryptoLib {
  static Future<void> createAccount(
      {String? privateKey, String? accountName}) async {
    try {
      Map<String, dynamic> keyPair;
      if (privateKey == null) {
        keyPair = await CryptoLib.genSM2KeyPair();
      } else {
        keyPair = await CryptoLib.loadSM2KeyPair(privateKey);
      }
      keyPair['accountName'] =
          accountName ?? "${keyPair['account'].toString().substring(16, 20)}";
      keyPair['algType'] = "sm2";
      await setStorageInfo(keyPair);
    } catch (e) {
      debugPrint(e as String?);
    }
  }

  //storage存放account等信息
  static Future<void> setStorageInfo(Map keyPair) async {
    var keyPairs = Storage.get("keyPairs") ?? [];
    bool isInPairs = false;
    for (int i = 0; i < keyPairs.length; i++) {
      if (keyPairs[i]['account'] == keyPair['account']) {
        isInPairs = true;
        break;
      }
    }

    ///不存在则加进来
    if (!isInPairs) keyPairs.add(keyPair);
    await Storage.set("keyPairs", keyPairs);
    await Storage.set("account", keyPair["account"]);
    await Storage.set("privateKey", keyPair["privateKey"]);
    await Storage.set("publicKey", keyPair["publicKey"]);
    await Storage.set("accountName", keyPair["accountName"]);
    await Storage.set("algType", keyPair["algType"]);
  }

  static String encryptKeyPairs(dynamic keyPairs, String password) {
    assert(keyPairs.length > 0);
    String code = CryptoLib.encipher(json.encode(keyPairs), password);
    return code;
  }

  static List decryptKeyPairs(String code, String password) {
    List keyPairs = json
        .decode(CryptoLib.decipher(code, password))
        ?.map((item) => Map.from(item))
        ?.toList();
    assert(keyPairs.length > 0);
    return keyPairs;
  }

  //取得homebox的
  // algorithm = "SHA256WithRSA" ,"SHA256withECDSA"
  static String? getAuthorizationCode(Object data,
      [CryptoAlgorithm algorithm = CryptoAlgorithm.ECC]) {
    switch (algorithm) {
      case CryptoAlgorithm.ECC:
        String sign = CryptoLib.sign(
            "$data", Storage.get("ecKeyPair")["privateKey"],
            algType: CryptoAlgorithm.ECC);
        String puk = Storage.get("ecKeyPair")["publicKey"];

        //asn.1 pub
        ASN1Sequence sequence = ASN1Sequence();
        ASN1Sequence sequenceOID = ASN1Sequence();
        var oid1 =
            ASN1ObjectIdentifier.fromIdentifierString('1.2.840.10045.2.1');
        //var oid1 = ASN1ObjectIdentifier.fromName('ecdsa');
        //debugPrint(oid11.encode().buffer.asUint8List());
        debugPrint(BufferLib.bufferToHexString(oid1.encode()));
        var oid21 = ASN1ObjectIdentifier.fromIdentifierString('1.3.132.0.10');
        var oid2 = ASN1ObjectIdentifier.fromName('secp256k1');
        debugPrint(BufferLib.bufferToHexString(oid21.encode()));
        debugPrint(BufferLib.bufferToHexString(oid2.encode()));
        sequenceOID.add(oid1);
        sequenceOID.add(oid2);
        sequence.add(sequenceOID);
        debugPrint(base64.decode(puk) as String?);
        var bitString =
            ASN1BitString(stringValues: base64.decode(puk).toList());
        sequence.add(bitString);
        Uint8List pubAsn1 = sequence.encode();
        String pubAsn1base64 = base64.encode(pubAsn1);
        debugPrint("data:$data");
        debugPrint("puk:$pubAsn1base64");
        debugPrint("sign:$sign");
        debugPrint("verify:${CryptoLib.verify("$data", puk, sign)}");
        String code = base64.encode(utf8.encode(json.encode({
          "data": data,
          "puk": pubAsn1base64,
          "sign": sign,
          "algorithm": "SHA256withECDSA"
        })));
        return code;
      default:
        return null;
    }
  }

  static String genAccount(String pukStrNoPress,
      {HashAlgorithm hashType = HashAlgorithm.SM3}) {
    assert(
        hashType == HashAlgorithm.SM3 || hashType == HashAlgorithm.Keccak256);
    List<int> pukBuffer =
        BufferLib.hexStringToBuffer(pukStrNoPress).sublist(1); //去除非压缩puk的前导0x04
    assert(pukBuffer.length == 64);
    late String hash;
    if (hashType == HashAlgorithm.SM3) {
      hash = HashLib.sm3(pukBuffer);
    } else {
      hash = HashLib.keccak256(pukBuffer);
    }
    String account = hash.substring(hash.length - 40);
    return account;
  }

  static String strinifyPrivateKey(PrivateKey pk) {
    final privateKey = pk as ECPrivateKey;
    return privateKey.d!.toRadixString(16);
  }

  static String strinifyPublicKey(PublicKey pk, {bool compress = true}) {
    final publicKey = pk as ECPublicKey;
    Uint8List publicKeyBuf = publicKey.Q!.getEncoded(compress);
    return BufferLib.bufferToHexString(publicKeyBuf);
  }

  static ECPrivateKey loadPrivateKey(String privateKeyHexStr,
      {CryptoAlgorithm algType = CryptoAlgorithm.SM2}) {
    final d = BigInt.parse(privateKeyHexStr, radix: 16);
    late ECDomainParameters param;
    switch (algType) {
      case CryptoAlgorithm.SM2:
        param = ECCurve_sm2p256v1();
        break;
      case CryptoAlgorithm.ECC:
        param = ECCurve_secp256k1();
        break;
      case CryptoAlgorithm.X25519:
      case CryptoAlgorithm.ECDH:
        throw "loadPublicKey函数仅支持sm2,ecc";
    }
    return new ECPrivateKey(d, param);
  }

  static String getPublicKeyByPrivateKey(String privateKeyHexStr,
      {CryptoAlgorithm algType = CryptoAlgorithm.SM2}) {
    final keyPair =
        CryptoLib.loadKeyPair(privateKeyHexStr, algType: CryptoAlgorithm.SM2);
    return keyPair['publicKey'];
  }

  static ECPublicKey loadPublicKey(String publicKeyHexStr,
      {CryptoAlgorithm algType = CryptoAlgorithm.SM2}) {
    late ECDomainParameters param;
    switch (algType) {
      case CryptoAlgorithm.SM2:
        param = ECCurve_sm2p256v1();
        break;
      case CryptoAlgorithm.ECC:
        param = ECCurve_secp256k1();
        break;
      case CryptoAlgorithm.X25519:
      case CryptoAlgorithm.ECDH:
        throw "loadPublicKey函数仅支持sm2,ecc";
    }

    if (publicKeyHexStr.length < 120) {
      List<int> pubKeyBuf = BufferLib.hexStringToBuffer(publicKeyHexStr);
      final Q = param.curve.decodePoint(pubKeyBuf);
      return ECPublicKey(Q, param);
    } else {
      final x = BigInt.parse(publicKeyHexStr.substring(2, 66), radix: 16);
      final y = BigInt.parse(publicKeyHexStr.substring(66), radix: 16);
      final Q = param.curve.createPoint(x, y);
      return ECPublicKey(Q, param);
    }
  }

  static Map<String, dynamic> loadSM2KeyPair(String privateKeyHexStr) {
    return loadKeyPair(privateKeyHexStr,
        algType: CryptoAlgorithm.SM2, hashType: HashAlgorithm.SM3);
  }

  static Map<String, dynamic> loadECKeyPair(String privateKeyHexStr) {
    return loadKeyPair(privateKeyHexStr,
        algType: CryptoAlgorithm.ECC, hashType: HashAlgorithm.Keccak256);
  }

  static Map<String, dynamic> loadKeyPair(String privateKeyHexStr,
      {CryptoAlgorithm algType = CryptoAlgorithm.SM2,
      HashAlgorithm hashType = HashAlgorithm.SM3}) {
    assert(algType == CryptoAlgorithm.SM2 || algType == CryptoAlgorithm.ECC);
    assert(
        hashType == HashAlgorithm.SM3 || hashType == HashAlgorithm.Keccak256);
    late ECDomainParameters param;
    switch (algType) {
      case CryptoAlgorithm.SM2:
        param = ECCurve_sm2p256v1();
        break;
      case CryptoAlgorithm.ECC:
        param = ECCurve_secp256k1();
        break;
      default:
        throw "must define valid algType!";
    }
    final privateKey = CryptoLib.loadPrivateKey(privateKeyHexStr);
    //ECPoint Q = scalar_multiple(privateKey.d!, param.G)!;
    ECPoint Q = (param.G * privateKey.d)!;
    final keypair = AsymmetricKeyPair(
        ECPublicKey(Q, param), ECPrivateKey(privateKey.d, param));
    final publicKey = keypair.publicKey;
    String pik = privateKey.d!.toRadixString(16);
    String puk = BufferLib.bufferToHexString(publicKey.Q!.getEncoded(false));
    String account = CryptoLib.genAccount(puk, hashType: hashType);
    return {"privateKey": pik, "publicKey": puk, "account": account};
  }

  static genSM2KeyPair() {
    return genKeyPair(
        algType: CryptoAlgorithm.SM2, hashType: HashAlgorithm.SM3);
  }

  static genECKeyPair() {
    return genKeyPair(
        algType: CryptoAlgorithm.ECC, hashType: HashAlgorithm.Keccak256);
  }

  static genX25519KeyPair() {
    return genKeyPair(algType: CryptoAlgorithm.X25519);
  }

  static genKeyPair(
      {CryptoAlgorithm algType = CryptoAlgorithm.SM2,
      hashType = HashAlgorithm.SM3}) {
    switch (algType) {
      case CryptoAlgorithm.SM2:
      case CryptoAlgorithm.ECC:
        late ECDomainParameters param;
        if (algType == CryptoAlgorithm.SM2) {
          param = ECCurve_sm2p256v1();
        } else {
          param = ECCurve_secp256k1();
        }
        var random = FortunaRandom();
        random.seed(KeyParameter(_seed(32)));
        var n = param.n;
        var nBitLength = n.bitLength;
        var d;
        do {
          d = random.nextBigInteger(nBitLength);
        } while (d == BigInt.zero || (d >= n));
        //ECPoint Q = scalar_multiple(d, param.G)!;
        ECPoint Q = (param.G * d)!;
        final keyPair =
            AsymmetricKeyPair(ECPublicKey(Q, param), ECPrivateKey(d, param));
        final privateKeyHexStr = keyPair.privateKey.d!.toRadixString(16);
        final publicKeyNoCompressHexStr =
            BufferLib.bufferToHexString(keyPair.publicKey.Q!.getEncoded(false));
        final ethAccount =
            CryptoLib.genAccount(publicKeyNoCompressHexStr, hashType: hashType);
        return {
          "privateKey": privateKeyHexStr,
          "publicKey": publicKeyNoCompressHexStr,
          "account": ethAccount
        };
      case CryptoAlgorithm.X25519:
        final keyPair = x22519.generateKeyPair();
        return {
          "privateKey": BufferLib.bufferToHexString(
              Uint8List.fromList(keyPair.privateKey)),
          "publicKey":
              BufferLib.bufferToHexString(Uint8List.fromList(keyPair.publicKey))
        };
      default:
        throw "must define valid algType!";
    }
  }

  static String encrypt(message, String publicKeyHexStr,
      {CryptoAlgorithm algType = CryptoAlgorithm.SM2, int cipherMode = 1}) {
    assert(algType == CryptoAlgorithm.SM2, "仅支持SM2非对称加密");
    late SM2Encryptor encryptor;
    ECPublicKey publicKey = loadPublicKey(publicKeyHexStr, algType: algType);
    PublicKeyParameter pubkeyParam = PublicKeyParameter(publicKey);
    String res = '';
    encryptor = SM2Encryptor();
    encryptor.init(pubkeyParam, cipherMode);
    if (message is String) {
      res = encryptor.encrypt(Uint8List.fromList(utf8.encode(message)));
    } else {
      res = encryptor.encrypt(Uint8List.fromList(message));
    }
    return res;
  }

  static String? decrypt(String cryptData, String privateKeyHexStr,
      {CryptoAlgorithm algType = CryptoAlgorithm.SM2, int cipherMode = 1}) {
    assert(algType == CryptoAlgorithm.SM2, "仅支持SM2非对称解密");
    late SM2Decryptor decryptor;
    ECPrivateKey privateKey =
        loadPrivateKey(privateKeyHexStr, algType: algType);
    PrivateKeyParameter privatekeyParam = PrivateKeyParameter(privateKey);
    decryptor = SM2Decryptor();
    decryptor.init(privatekeyParam, cipherMode);

    return decryptor.decrypt(cryptData);
  }

  static String sign(message, String privateKeyHexStr,
      {CryptoAlgorithm algType = CryptoAlgorithm.SM2,
      HashAlgorithm hashType = HashAlgorithm.SM3,
      bool needHash = true,
      bool random = true}) {
    ECPrivateKey privateKey =
        loadPrivateKey(privateKeyHexStr, algType: algType);
    assert(hashType == HashAlgorithm.SHA256 || hashType == HashAlgorithm.SM3);
    assert(random == true, '目前暂不支持random==false');
    late Signer singer;
    if (hashType == HashAlgorithm.SHA256) {
      singer = ECDSASigner(
          needHash ? SHA256Digest() : null, null); //new Mac('SHA-256/HMAC'));
    } else {
      singer = SM2ECDSASigner(
          needHash ? SM3Digest() : null, null); // new Mac('SM3/HMAC'));
    }
    var privParams =
        PrivateKeyParameter(ECPrivateKey(privateKey.d, privateKey.parameters));
    signParams() => ParametersWithRandom(privParams, SecureRandom());
    singer.init(true, signParams());
    ECSignature signature;
    if (message is String) {
      signature =
          singer.generateSignature(Uint8List.fromList(utf8.encode(message)))
              as ECSignature;
    } else {
      signature =
          singer.generateSignature(Uint8List.fromList(message)) as ECSignature;
    }
    //debugPrint("sign:$signature,message:$message");
    final x_s = signature.r.toRadixString(16);
    final y_s = signature.s.toRadixString(16);

    return x_s.padLeft(64, '0') + y_s.padLeft(64, '0');
  }

  static bool verify(message, String publicKeyHexStr, String strSignature,
      {CryptoAlgorithm algType = CryptoAlgorithm.SM2,
      HashAlgorithm hashType = HashAlgorithm.SM3,
      needHash = true}) {
    ECPublicKey publicKey = loadPublicKey(publicKeyHexStr, algType: algType);
    late Signer verifySinger;
    assert(hashType == HashAlgorithm.SHA256 || hashType == HashAlgorithm.SM3);
    if (hashType == HashAlgorithm.SHA256) {
      verifySinger = ECDSASigner(
          needHash ? SHA256Digest() : null, null); //new Mac('SHA-256/HMAC'));
    } else {
      verifySinger = SM2ECDSASigner(
          needHash ? SM3Digest() : null, null); //new Mac('SM3/HMAC'));
    }
    var pubkeyParam =
        PublicKeyParameter(ECPublicKey(publicKey.Q, publicKey.parameters));

    final str_r = strSignature.substring(0, 64);
    final str_s = strSignature.substring(64, 128);
    final r = BigInt.parse(str_r, radix: 16);
    final s = BigInt.parse(str_s, radix: 16);

    ECSignature signature = ECSignature(r, s);
    verifySinger.init(false, pubkeyParam);
    if (message is String) {
      return verifySinger.verifySignature(
          Uint8List.fromList(utf8.encode(message)), signature);
    } else {
      return verifySinger.verifySignature(
          Uint8List.fromList(message), signature);
    }
  }

/*
  static String sign(String msg, String privateKeyHexStr,
      {CryptoAlgorithm algType: CryptoAlgorithm.SM2,
      HashAlgorithm hashType: HashAlgorithm.SHA256,
      bool der: false}) {
    assert(
        hashType == HashAlgorithm.SHA256 || hashType == HashAlgorithm.SHA512);
    assert([CryptoAlgorithm.ECC, CryptoAlgorithm.SM2].contains(algType));
    switch (algType) {
      case CryptoAlgorithm.ECC:
        switch (hashType) {
          case HashAlgorithm.SHA256:
            if (der) {
              String sig = utf8.decode(CryptoLib.loadPrivateKey(
                      privateKeyHexStr)
                  .createSHA256Signature(Uint8List.fromList(utf8.encode(msg))));
              debugPrint(sig);
              BigInt r = BigInt.parse("0x${sig.substring(0, 64)}");
              BigInt s1 = BigInt.parse(sig.substring(64), radix: 16);
              debugPrint(s1);
              BigInt s = BigInt.parse("0x${sig.substring(64)}");
              debugPrint(s);
              ASN1Sequence sequence = ASN1Sequence();
              sequence.add(ASN1Integer(r));
              sequence.add(ASN1Integer(s));
              debugPrint("sigNew:${sequence.encode()}");
              return base64Encode(sequence.encode());
            } else {
              return BufferLib.bufferToHexString(CryptoLib.loadPrivateKey(
                      privateKeyHexStr)
                  .createSHA256Signature(Uint8List.fromList(utf8.encode(msg))));
            }
          case HashAlgorithm.SHA512:
            return BufferLib.bufferToHexString(CryptoLib.loadPrivateKey(
                    privateKeyHexStr)
                .createSHA512Signature(Uint8List.fromList(utf8.encode(msg))));
          default:
            throw Exception("hash function must use hashType(sha256/sha512)");
        }
      case CryptoAlgorithm.SM2:
        //使用sha512
        return sm2.privateSign(privateKeyHexStr, msg);
      default:
        throw Exception("sign function must use algType(EC/ETH/SM2/RSA)");
    }
  }
  static bool verify(String msg, String publicKeyHexStr, String sign,
      {CryptoAlgorithm algType: CryptoAlgorithm.SM2,
      HashAlgorithm hashType: HashAlgorithm.SHA256,
      bool der: false}) {
    assert(
        hashType == HashAlgorithm.SHA256 || hashType == HashAlgorithm.SHA512);
    assert([CryptoAlgorithm.ECC, CryptoAlgorithm.SM2].contains(algType));
    switch (algType) {
      case CryptoAlgorithm.ECC:
        switch (hashType) {
          case HashAlgorithm.SHA256:
            if (der) {
              ASN1Parser parser = ASN1Parser(base64Decode(sign));
              List<ASN1Object>? rs =
                  (parser.nextObject() as ASN1Sequence).elements;
              BigInt? r = (rs![0] as ASN1Integer).integer;
              BigInt? s = (rs[1] as ASN1Integer).integer;
              Uint8List sigBuffer =
                  utf8.encode(r!.toRadixString(16) + s!.toRadixString(16))
                      as Uint8List;
              return CryptoLib.loadPublicKey(publicKeyHexStr)
                  .verifySHA256Signature(
                      Uint8List.fromList(utf8.encode(msg)), sigBuffer);
            } else {
              return CryptoLib.loadPublicKey(publicKeyHexStr)
                  .verifySHA256Signature(Uint8List.fromList(utf8.encode(msg)),
                      BufferLib.hexStringToBuffer(sign) as Uint8List);
            }
          case HashAlgorithm.SHA512:
            return CryptoLib.loadPublicKey(publicKeyHexStr)
                .verifySHA512Signature(Uint8List.fromList(utf8.encode(msg)),
                    BufferLib.hexStringToBuffer(sign) as Uint8List);
          default:
            throw Exception("hash function must use hashType(sha256/sha512)");
        }
      case CryptoAlgorithm.SM2:
        // switch (hashType) {
        //   case HashAlgorithm.SHA256:
        //     return SM2PublicKey.fromString(publicKey).verifySHA256Signature(
        //         Uint8List.fromList(utf8.encode(msg)), base64Decode(sign));
        //   case HashAlgorithm.SHA512:
        //     return SM2PublicKey.fromString(publicKey).verifySHA512Signature(
        //         Uint8List.fromList(utf8.encode(msg)), base64Decode(sign));
        //   default:
        //     throw Exception("hash function must use hashType(sha256/sha512)");
        // }
        //使用sha512
        return sm2.publicVerify(publicKeyHexStr, msg, sign);
      default:
        throw Exception("verify function must use algType(EC/RSA)");
    }
  }

  // static ecEncrypt(String msg, String publicKeyHexString) {
  //   var a = ECElGamalEncryptor();
  //   var ecEncryptor = ECEncryptor();
  // }
  static String ecdh(String privateHexStr, String publicHexStr) {
    return "";
     var e = ECDHBasicAgreement();
    // e.init(loadPrivateKey(privateHexStr));
    // BigInt b = e.calculateAgreement(loadPublicKey(publicHexStr));
    // return b.toRadixString(16);
    // var k = ECDHKDFParameters(
    //     loadPrivateKey(privateHexStr), loadPublicKey(publicHexStr));
  }

  static String encipher1(String msg, String passwd) {
    var cipher = StreamCipher('AES/CBC');
    cipher.init(true, KeyParameter(Uint8List.fromList(utf8.encode(passwd))));
    var result = cipher.process(Uint8List.fromList(utf8.encode(msg)));
    return base64.encode(result);
  }

  static String decipher1(String msg, String passwd) {
    var cipher = StreamCipher('AES/CBC');
    cipher.init(true, KeyParameter(Uint8List.fromList(utf8.encode(passwd))));
    var result = cipher.process(Uint8List.fromList(base64.decode(msg)));
    return utf8.decode(result);
  }
*/

  static String encipher(String msg, String passwd) {
    final md5Passwd = HashLib.md5(utf8.encode(passwd));
    final key32 = base64.encode(BufferLib.hexStringToBuffer(md5Passwd));
    final twiceMd5Passwd = HashLib.md5(utf8.encode(md5Passwd));
    final iv = base64.encode(BufferLib.hexStringToBuffer(twiceMd5Passwd));
    debugPrint("iv>>>>$iv");
    //var aes = AesCrypt(key: key32, padding: PaddingAES.pkcs7);
    //var emsg = aes.ctr.encrypt(inp: msg, iv: iv); //Encrypt.
    //final ctrAes = PaddedBlockCipher('AES/CTR/PKCS7');
    final ctrAes = CTRStreamCipher(AESEngine());
    ctrAes.init(
        true,
        ParametersWithIV(
            KeyParameter(
                Uint8List.fromList(BufferLib.hexStringToBuffer(md5Passwd))),
            Uint8List.fromList(BufferLib.hexStringToBuffer(twiceMd5Passwd))));

    var inter = ctrAes.process(utf8.encode(msg) as Uint8List);
    return base64.encode(inter);
  }

  static String decipher(String emsg, String passwd) {
    final md5Passwd = HashLib.md5(utf8.encode(passwd));
    final key32 = base64.encode(BufferLib.hexStringToBuffer(md5Passwd));
    //final twiceMd5Passwd = HashLib.md5(BufferLib.hexStringToBuffer(md5Passwd));
    final twiceMd5Passwd = HashLib.md5(utf8.encode(md5Passwd));
    final iv = base64.encode(BufferLib.hexStringToBuffer(twiceMd5Passwd));
    debugPrint("iv<<<<<$iv");
    final ctrAes = CTRStreamCipher(AESEngine());
    ctrAes.init(
        false,
        ParametersWithIV(
            KeyParameter(
                Uint8List.fromList(BufferLib.hexStringToBuffer(md5Passwd))),
            Uint8List.fromList(BufferLib.hexStringToBuffer(twiceMd5Passwd))));
    var inter = ctrAes.process(base64.decode(emsg));
    return utf8.decode(inter);
  }

  static String getAuthBase64Header(
      {required String privateKeyHexStr,
      required String publicKeyHexStr,
      required String account,
      CryptoAlgorithm algType = CryptoAlgorithm.SM2}) {
    assert(algType == CryptoAlgorithm.SM2 || algType == CryptoAlgorithm.ECC);
    String type;
    if (algType == CryptoAlgorithm.SM2) {
      type = "sm2";
    } else {
      type = "ec";
    }
    final signature =
        CryptoLib.sign(account, privateKeyHexStr, algType: CryptoAlgorithm.SM2);
    final obj = {
      "account": account,
      "signature": signature,
      "publicKey": publicKeyHexStr,
      "algType": type
    };
    return base64.encode(utf8.encode(json.encode(obj)));
  }

  static bool verifyAuthBase64HeaderWithAccount(
      String base64Header, String account0) {
    var obj = json.decode(utf8.decode(base64.decode(base64Header)));
    debugPrint(obj);
    final signature = obj['signature'];
    final account = obj['account'];
    final publicKeyHexStr = obj['publicKey'];
    final type = obj['algType'];
    assert(type == "sm2" || type == "ec");
    late CryptoAlgorithm algType;
    if (type == "sm2") {
      algType = CryptoAlgorithm.SM2;
    } else {
      algType = CryptoAlgorithm.ECC;
    }
    if (account != account0) return false;
    final v1 =
        CryptoLib.verify(account, publicKeyHexStr, signature, algType: algType);
    final v2 = CryptoLib.genAccount(CryptoLib.strinifyPublicKey(
            CryptoLib.loadPublicKey(publicKeyHexStr),
            compress: false)) ==
        account;
    return v1 && v2;
  }

  static String safeSendWithECDH(
      {required String msg,
      required String privateKeyHexStr,
      required String publicKeyHexStr,
      required String account,
      required String otherPublicKeyHexStr,
      CryptoAlgorithm algType = CryptoAlgorithm.SM2}) {
    try {
      assert(algType == CryptoAlgorithm.SM2 || algType == CryptoAlgorithm.ECC);
      String type;
      if (algType == CryptoAlgorithm.SM2) {
        type = "sm2";
      } else {
        type = "ec";
      }
      //生成协商密钥
      final sharedKey = CryptoLib.sharedKey(
          privateKeyHexStr, otherPublicKeyHexStr,
          algType: CryptoAlgorithm.ECDH, ecdhType: algType);
      debugPrint("shardKey:$sharedKey");
      final msgCipher = encipher(msg, sharedKey);
      debugPrint("msg:$msg");
      debugPrint("msgCipher:$msgCipher");
      //用自己的私钥签名
      final signature = CryptoLib.sign(json.encode(msgCipher), privateKeyHexStr,
          algType: algType);
      final obj = {
        "msg": msgCipher,
        "signature": signature,
        "publicKey": publicKeyHexStr,
        "account": account,
        "algType": type
      };
      return base64.encode(utf8.encode(json.encode(obj)));
    } catch (e) {
      throw "Error on safeSendWithECDH!,detail is $e";
    }
  }

  static String safeSendWithX25519(
      {required String msg,
      required String privateKeyHexStr,
      required String publicKeyHexStr,
      required String account,
      required String selfX25519PrivateKeyHexStr,
      required String selfX25519PublicKeyHexStr,
      required String otherX25519PublicKeyHexStr,
      CryptoAlgorithm algType = CryptoAlgorithm.SM2}) {
    try {
      assert(algType == CryptoAlgorithm.SM2 || algType == CryptoAlgorithm.ECC);
      String type;
      if (algType == CryptoAlgorithm.SM2) {
        type = "sm2";
      } else {
        type = "ec";
      }
      //生成协商密钥
      final sharedKey = CryptoLib.sharedKey(
          selfX25519PrivateKeyHexStr, otherX25519PublicKeyHexStr,
          algType: CryptoAlgorithm.X25519);
      //确保加密密钥长度是8的倍数
      assert(sharedKey.length % 8 == 0);
      final msgCipher = encipher(msg, sharedKey);

      //用自己的私钥签名
      final signature = CryptoLib.sign(json.encode(msgCipher), privateKeyHexStr,
          algType: algType);
      final obj = {
        "msg": msgCipher,
        "signature": signature,
        "X25519PublicKey": selfX25519PublicKeyHexStr,
        "publicKey": publicKeyHexStr,
        "account": account,
        "algType": type
      };
      return base64.encode(utf8.encode(json.encode(obj)));
    } catch (e) {
      throw "Error on safeSendWithX25519!,detail is $e";
    }
  }

  static String safeRecieveWithECDH({
    required String objStr,
    required String selfPrivateKeyHexStr,
  }) {
    try {
      //转化为sigObj对象
      final obj = json.decode(utf8.decode(base64.decode(objStr)));
      String otherPublicKeyHexStr = obj['publicKey'];
      //验证数据完整性
      var algType;
      if (obj['algType'] == 'sm2') {
        algType = CryptoAlgorithm.SM2;
      } else {
        algType = CryptoAlgorithm.ECC;
      }
      //生成协商密钥
      final sharedKey = CryptoLib.sharedKey(
          selfPrivateKeyHexStr, otherPublicKeyHexStr,
          algType: CryptoAlgorithm.ECDH, ecdhType: algType);
      assert(verify(json.encode(obj["msg"]), obj["publicKey"], obj["signature"],
          algType: algType));
      debugPrint("通过签名验证！！");
      //验证publicKey与account的一致性
      final pukStrNoPress = CryptoLib.strinifyPublicKey(
          CryptoLib.loadPublicKey(obj["publicKey"], algType: algType),
          compress: false);
      assert(obj["account"] == CryptoLib.genAccount(pukStrNoPress));
      debugPrint("通过account合法性验证");
      //解析数据串中的加密串和密文，二次对称解密
      String msg = decipher(obj["msg"], sharedKey);
      return msg;
    } catch (e) {
      throw "Error on safeRecieveWithECDH!,detail is $e";
    }
  }

  static String safeRecieveWithX25519({
    required String objStr,
    required String selfX25519PrivateKeyHexStr,
  }) {
    try {
      //转化为sigObj对象
      final obj = json.decode(utf8.decode(base64.decode(objStr)));
      String otherX25519PublicKeyHexStr = obj['X25519PublicKey'];
      //生成协商密钥
      final sharedKey = CryptoLib.sharedKey(
          selfX25519PrivateKeyHexStr, otherX25519PublicKeyHexStr);
      //验证数据完整性
      var algType;
      if (obj['algType'] == 'sm2') {
        algType = CryptoAlgorithm.SM2;
      } else {
        algType = CryptoAlgorithm.ECC;
      }
      assert(verify(json.encode(obj["msg"]), obj["publicKey"], obj["signature"],
          algType: algType));
      debugPrint("通过签名验证！！");
      //验证publicKey与account的一致性
      final pukStrNoPress = CryptoLib.strinifyPublicKey(
          CryptoLib.loadPublicKey(obj["publicKey"], algType: algType),
          compress: false);
      assert(obj["account"] == CryptoLib.genAccount(pukStrNoPress));
      debugPrint("通过account合法性验证");
      //解析数据串中的加密串和密文，二次对称解密
      assert(sharedKey.length % 8 == 0);
      String msg = decipher(obj["msg"], sharedKey);
      return msg;
    } catch (e) {
      throw FormatException("Error on safeRecieveWidthX25519!,detail is $e");
    }
  }

  static String sharedKey(String selfPrivateKey, String otherPublicKey,
      {CryptoAlgorithm algType = CryptoAlgorithm.X25519,
      CryptoAlgorithm? ecdhType}) {
    switch (algType) {
      case CryptoAlgorithm.X25519:
        Uint8List sharedKey = x22519.X25519(
            BufferLib.hexStringToBuffer(selfPrivateKey),
            BufferLib.hexStringToBuffer(otherPublicKey));
        return BufferLib.bufferToHexString(sharedKey);
      case CryptoAlgorithm.ECDH:
        if (ecdhType == null) {
          throw "sharedKey函数algType为ECDH时，ecdhType必须指定";
        }
        final ecdh = ECDHBasicAgreement()
          ..init(CryptoLib.loadPrivateKey(selfPrivateKey, algType: ecdhType));
        return ecdh
            .calculateAgreement(
                CryptoLib.loadPublicKey(otherPublicKey, algType: ecdhType))
            .toRadixString(16);
      default:
        throw "sharedKey函数仅支持x25519，ecdh";
    }
  }
}

Uint8List _seed(length) {
  var random = Random.secure();
  var seed = List<int>.generate(length, (_) => random.nextInt(256));
  return Uint8List.fromList(seed);
}
