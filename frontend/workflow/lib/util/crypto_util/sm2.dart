import 'dart:convert';
import 'dart:math';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:pointycastle/export.dart';
import 'package:pointycastle/src/ec_standard_curve_constructor.dart';
import 'package:pointycastle/src/registry/registry.dart';
import 'package:pointycastle/src/utils.dart' as utils;

import '../data_util/data_util.dart';
import 'crypto_lib.dart';
import 'hash_lib.dart';

bool _testBit(BigInt i, int n) {
  return (i & (BigInt.one << n)) != BigInt.zero;
}

class ECCurve_sm2p256v1 extends ECDomainParametersImpl {
  static final FactoryConfig factoryConfig = StaticFactoryConfig(
      ECDomainParameters, 'sm2p256v1', () => ECCurve_sm2p256v1());

  factory ECCurve_sm2p256v1() => constructFpStandardCurve(
      'sm2p256v1', ECCurve_sm2p256v1._make,
      q: BigInt.parse(
          'FFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000FFFFFFFFFFFFFFFF',
          radix: 16),
      a: BigInt.parse(
          'FFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000FFFFFFFFFFFFFFFC',
          radix: 16),
      b: BigInt.parse(
          '28E9FA9E9D9F5E344D5A9E4BCF6509A7F39789F515AB8F92DDBCBD414D940E93',
          radix: 16),
      g: BigInt.parse(
          '0432C4AE2C1F1981195F9904466A39C9948FE30BBFF2660BE1715A4589334C74C7BC3736A2F4F6779C59BDCEE36B692153D0A9877CC62A474002DF32E52139F0A0',
          radix: 16),
      n: BigInt.parse(
          'FFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFF7203DF6B21C6052B53BBF40939D54123',
          radix: 16),
      h: BigInt.parse('1', radix: 16),
      seed: null) as ECCurve_sm2p256v1;

  static ECCurve_sm2p256v1 _make(String domainName, ECCurve curve, ECPoint G,
          BigInt n, BigInt _h, List<int>? seed) =>
      ECCurve_sm2p256v1._super(domainName, curve, G, n, _h, seed);

  ECCurve_sm2p256v1._super(String domainName, ECCurve curve, ECPoint G,
      BigInt n, BigInt _h, List<int>? seed)
      : super(domainName, curve, G, n, _h, seed);
}

// BigInt theP = BigInt.parse(
//     "FFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000FFFFFFFFFFFFFFFF",
//     radix: 16);
// BigInt theN = BigInt.parse(
//     "FFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFF7203DF6B21C6052B53BBF40939D54123",
//     radix: 16);

// bool isOnCurve(ECPoint point) {
//   final x = point.x!.toBigInteger()!;
//   final y = point.y!.toBigInteger()!;
//   final rs = (y * y -
//           x * x * x -
//           point.curve.a!.toBigInteger()! * x -
//           point.curve.b!.toBigInteger()!) %
//       theP;
//   return rs == BigInt.from(0);
// }

// BigInt inverse_mod(BigInt k, BigInt p) {
//   if (k == 0) {
//     throw IntegerDivisionByZeroException();
//   }
//   if (k < BigInt.from(0)) {
//     return p - inverse_mod(-k, p);
//   }
//   var s = [BigInt.from(0), BigInt.from(1), BigInt.from(1)];
//   var t = [BigInt.from(1), BigInt.from(0), BigInt.from(0)];
//   var r = [p, k, k];
//   while (r[0] != BigInt.from(0)) {
//     var quotient = r[2] ~/ r[0];
//     r[1] = r[2] - quotient * r[0];
//     r[2] = r[0];
//     r[0] = r[1];
//     s[1] = s[2] - quotient * s[0];
//     s[2] = s[0];
//     s[0] = s[1];
//     t[1] = t[2] - quotient * t[0];
//     t[2] = t[0];
//     t[0] = t[1];
//   }
//   final gcd = r[2];
//   final x = s[2];
//   final y = t[2];
//   assert(gcd == BigInt.from(1));
//   assert((k * x) % p == BigInt.from(1));
//   return x % p;
// }

// ECPoint? point_neg(ECPoint point) {
//   assert(isOnCurve(point));
//   if (point == null) {
//     return null;
//   }
//   final x = point.x!.toBigInteger()!;
//   final y = point.y!.toBigInteger()!;
//   final result = point.curve.createPoint(x, -y % theP);
//   assert(isOnCurve(result));
//   return result;
// }

// ECPoint? point_add(ECPoint? point1, ECPoint? point2) {
//   if (point1 == null) {
//     return point2;
//   }
//   if (point2 == null) {
//     return point1;
//   }
//   assert(isOnCurve(point1));
//   assert(isOnCurve(point2));
//   final x1 = point1.x!.toBigInteger()!;
//   final y1 = point1.y!.toBigInteger()!;
//   final x2 = point2.x!.toBigInteger()!;
//   final y2 = point2.y!.toBigInteger()!;
//   if (x1 == x2 && y1 != y2) {
//     return null;
//   }
//   var m;
//   if (x1 == x2) {
//     m = (BigInt.from(3) * x1 * x1 + point1.curve.a!.toBigInteger()!) *
//         inverse_mod(BigInt.from(2) * y1, theP);
//   } else {
//     m = (y1 - y2) * inverse_mod(x1 - x2, theP);
//   }
//   final x3 = m * m - x1 - x2;
//   final y3 = y1 + m * (x3 - x1);
//   ECPoint result = point1.curve.createPoint(x3 % theP, -y3 % theP);
//   assert(isOnCurve(result));
//   return result;
// }

// ECPoint? scalar_multiple(BigInt k, ECPoint point) {
//   assert(isOnCurve(point));
//   if (k % theN == 0 || point == null) {
//     return null;
//   }
//   if (k < BigInt.from(0)) {
//     return scalar_multiple(-k, point_neg(point)!);
//   }
//   var result = null;
//   var addend = point;
//   while (k > BigInt.from(0)) {
//     if (k & BigInt.from(1) > BigInt.from(0)) {
//       result = point_add(result, addend);
//     }
//     addend = point_add(addend, addend)!;
//     k >>= 1;
//   }
//   assert(isOnCurve(result));
//   return result;
// }

class SM2Encryptor {
  ECPublicKey? _key;
  late SecureRandom _random;
  late int _cipherMode;

  /// Process a single EC [point] using the basic ElGamal algorithm.
  String encrypt(Uint8List messageBuffer) {
    if (_key == null) {
      throw StateError('ECElGamalEncryptor is not initialised');
    }
    var key = _key!;
    var ec = key.parameters!;
    var keypair = CryptoLib.genSM2KeyPair();
    var k = BigInt.parse(keypair['privateKey'], radix: 16);
    // c1 = k * G
    var c1 = keypair['publicKey'];
    if (c1.length > 128) {
      c1 = c1.substring(c1.length - 128);
    }
    //c1 = '04' + c1;

    // (x2,y2) = k * publicKey
    ECPoint? p = _key!.Q! * k;
    List<int> x2 = BufferLib.hexStringToBuffer(
        p!.x!.toBigInteger()!.toRadixString(16).padLeft(64, '0'));
    List<int> y2 = BufferLib.hexStringToBuffer(
        p.y!.toBigInteger()!.toRadixString(16).padLeft(64, '0'));

    // c3 = hash(x2 || msg || y2)
    String c3 = HashLib.sm3([...x2, ...messageBuffer, ...y2]);

    int ct = 1;
    int offset = 0;
    List<int> t = []; //256位
    List<int> z = [...x2, ...y2];
    Function nextT = () {
      // (1) Hai = hash(z || ct)
      // (2) ct++
      t = BufferLib.hexStringToBuffer(HashLib.sm3([
        ...z,
        ct >> 24 & 0x00ff,
        ct >> 16 & 0x00ff,
        ct >> 8 & 0x00ff,
        ct & 0x00ff
      ]));
      ct++;
      offset = 0;
    };
    nextT(); //先生成Ha1
    int len = messageBuffer.length;
    for (int i = 0; i < len; i++) {
      // t= Ha1 || Ha2 || Ha3 || Ha4
      if (offset == t.length) {
        nextT();
      }
      // c2 = msg ^ t
      messageBuffer[i] ^= t[offset++] & 0xff;
    }
    final c2 = BufferLib.bufferToHexString(messageBuffer);

    // _cipherMode=1 为标准模式 c1+c3+c2
    return _cipherMode == 0 ? c1 + c2 + c3 : c1 + c3 + c2;
  }

  void init(CipherParameters params, [int cipherMode = 1]) {
    AsymmetricKeyParameter akparams;
    if (params is ParametersWithRandom) {
      akparams = params.parameters as AsymmetricKeyParameter<AsymmetricKey>;
      _random = params.random;
    } else {
      akparams = params as AsymmetricKeyParameter<AsymmetricKey>;
      _random = SecureRandom();
    }
    var k = akparams.key as ECAsymmetricKey;
    if (k is! ECPublicKey) {
      throw ArgumentError('ECPublicKey is required for encryption.');
    }
    _key = k;
    _cipherMode = cipherMode;
  }
}

class SM2Decryptor {
  ECPrivateKey? _key;
  late SecureRandom _random;
  late int _cipherMode;

  /// Process a single EC [point] using the basic ElGamal algorithm.
  String? decrypt(String encryptData) {
    if (_key == null) {
      throw StateError('ECElGamalEncryptor is not initialised');
    }
    if (encryptData.startsWith('04')) {
      encryptData = encryptData.substring(2);
    }

    var key = _key!;
    var ec = key.parameters!;
    var d = key.d;

    String c3 = encryptData.substring(128, 64 + 128);
    String c2 = encryptData.substring(128 + 64);

    if (_cipherMode == 0) {
      c3 = encryptData.substring(encryptData.length - 64);
      c2 = encryptData.substring(128, encryptData.length - 64);
    }
    List<int> msg = BufferLib.hexStringToBuffer(c2);
    String c1 = encryptData.substring(0, 128);
    ECPublicKey publicKey =
        CryptoLib.loadPublicKey('04' + c1, algType: CryptoAlgorithm.SM2);
    ECPoint? p = publicKey.Q! * d;
    List<int> x2 = BufferLib.hexStringToBuffer(
        p!.x!.toBigInteger()!.toRadixString(16).padLeft(64, '0'));
    List<int> y2 = BufferLib.hexStringToBuffer(
        p.y!.toBigInteger()!.toRadixString(16).padLeft(64, '0'));

    int ct = 1;
    int offset = 0;
    List<int> t = []; //256位
    List<int> z = [...x2, ...y2];
    Function nextT = () {
      // (1) Hai = hash(z || ct)
      // (2) ct++
      t = BufferLib.hexStringToBuffer(HashLib.sm3([
        ...z,
        ct >> 24 & 0x00ff,
        ct >> 16 & 0x00ff,
        ct >> 8 & 0x00ff,
        ct & 0x00ff
      ]));
      ct++;
      offset = 0;
    };
    nextT(); //先生成Ha1
    int len = msg.length;
    for (int i = 0; i < len; i++) {
      // t= Ha1 || Ha2 || Ha3 || Ha4
      if (offset == t.length) {
        nextT();
      }
      // c2 = msg ^ t
      msg[i] ^= t[offset++] & 0xff;
    }
    // c3 = hash(x2 || msg || y2)
    String checkC3 = HashLib.sm3([...x2, ...msg, ...y2]);
    if (checkC3 == c3.toLowerCase()) {
      try {
        return utf8.decode(msg);
      } catch (_) {
        return null;
      }
    } else {
      return null;
    }
  }

  void init(CipherParameters params, [int cipherMode = 1]) {
    AsymmetricKeyParameter akparams;
    if (params is ParametersWithRandom) {
      akparams = params.parameters as AsymmetricKeyParameter<AsymmetricKey>;
      _random = params.random;
    } else {
      akparams = params as AsymmetricKeyParameter<AsymmetricKey>;
      _random = SecureRandom();
    }
    var k = akparams.key as ECAsymmetricKey;
    if (k is! ECPrivateKey) {
      throw ArgumentError('ECPrivateKey is required for decryption.');
    }
    _key = k;
    _cipherMode = cipherMode;
  }
}

class SM2ECDSASigner implements Signer {
  /// Intended for internal use.
  // ignore: non_constant_identifier_names
  static final FactoryConfig factoryConfig = DynamicFactoryConfig.regex(
      Signer, r'^(.+)/(DET-)?ECDSA$', (_, final Match match) {
    // ignore: omit_local_variable_types
    final String? digestName = match.group(1);
    // ignore: omit_local_variable_types
    final bool withMac = match.group(2) != null;
    return () {
      var underlyingDigest = Digest(digestName!);
      var mac = withMac ? Mac('$digestName/HMAC') : null;
      return SM2ECDSASigner(underlyingDigest, mac);
    };
  });

  ECPublicKey? _pbkey;
  ECPrivateKey? _pvkey;
  SecureRandom? _random;
  final Digest? _digest;
  final Mac? _kMac;

  /// If [_digest] is not null it is used to hash the message before signing and verifying, otherwise, the message needs to be
  /// hashed by the user of this [ECDSASigner] object.
  /// If [_kMac] is not null, RFC 6979 is used for k calculation with the given [Mac]. Keep in mind that, to comply with
  /// RFC 69679, [_kMac] must be HMAC with the same digest used to hash the message.
  SM2ECDSASigner([this._digest, this._kMac]);

  @override
  String get algorithmName =>
      '${_digest!.algorithmName}/${_kMac == null ? '' : 'DET-'}ECDSA';

  @override
  void reset() {}

  /// Init this [Signer]. The [params] argument can be:
  /// -A [ParametersWithRandom] containing a [PrivateKeyParameter] or a raw [PrivateKeyParameter] for signing
  /// -An [PublicKeyParameter] for verifying.
  @override
  void init(bool forSigning, CipherParameters params) {
    _pbkey = _pvkey = null;

    if (forSigning) {
      PrivateKeyParameter pvparams;

      if (params is ParametersWithRandom) {
        _random = params.random;
        pvparams = params.parameters as PrivateKeyParameter<PrivateKey>;
      } else if (_kMac != null) {
        _random = null;
        pvparams = params as PrivateKeyParameter<PrivateKey>;
      } else {
        _random = SecureRandom();
        pvparams = params as PrivateKeyParameter<PrivateKey>;
      }

      if (pvparams is! PrivateKeyParameter) {
        throw ArgumentError(
            'Unsupported parameters type ${pvparams.runtimeType}: should be PrivateKeyParameter');
      }
      _pvkey = pvparams.key as ECPrivateKey;
    } else {
      PublicKeyParameter pbparams;

      pbparams = params as PublicKeyParameter<PublicKey>;

      if (pbparams is! PublicKeyParameter) {
        throw ArgumentError(
            'Unsupported parameters type ${pbparams.runtimeType}: should be PublicKeyParameter');
      }
      _pbkey = pbparams.key as ECPublicKey;
    }
  }

  @override
  Signature generateSignature(Uint8List message) {
    message = _hashMessageIfNeeded(message);
    BigInt m = BigInt.parse(BufferLib.bufferToHexString(message), radix: 16);
    var n = _pvkey!.parameters!.n;
    var d = _pvkey!.d!;
    BigInt r;
    BigInt s;
    BigInt? k;

    dynamic kCalculator;
    if (_kMac != null) {
      kCalculator = _RFC6979KCalculator(_kMac!, n, d, message);
    } else {
      kCalculator = _RandomKCalculator(n, _random!);
    }
    while (true) {
      k = kCalculator.nextK() as BigInt?;
      var kg = (_pvkey!.parameters!.G * k)!;
      r = (m + kg.x!.toBigInteger()!) % n;
      if (r == BigInt.zero) {
        continue;
      }
      if (r + k! == n) {
        continue;
      }
      final t1 = (BigInt.one + d).modInverse(n);
      final t2 = (k - (r * d)) % n;
      s = (t1 * t2) % n;
      if (s != BigInt.zero) {
        break;
      }
    }
    debugPrint("d:${d.toRadixString(16)}");
    debugPrint("m:${m.toRadixString(16)}");
    debugPrint("k:${k.toRadixString(16)}");
    debugPrint("n:${n.toRadixString(16)}");
    debugPrint("r:${r.toRadixString(16)}");
    debugPrint("s:${s.toRadixString(16)}");
    return ECSignature(r, s);
  }

  @override
  bool verifySignature(Uint8List message, covariant ECSignature signature) {
    message = _hashMessageIfNeeded(message);
    BigInt m = BigInt.parse(BufferLib.bufferToHexString(message), radix: 16);

    var n = _pbkey!.parameters!.n;

    var r = signature.r;
    var s = signature.s;

    // r in the range [1,n-1]
    if (r.compareTo(BigInt.one) < 0 || r.compareTo(n) >= 0) {
      return false;
    }

    // s in the range [1,n-1]
    if (s.compareTo(BigInt.one) < 0 || s.compareTo(n) >= 0) {
      return false;
    }

    final t = (r + s) % n;
    if (t == BigInt.zero) {
      return false;
    }

    var G = _pbkey!.parameters!.G;
    var Q = _pbkey!.Q!;

    final q = (G * s)! + (Q * t);
    final R = (m + q!.x!.toBigInteger()!) % n;
    print('<--q:${m.toRadixString(16)}');
    print('<--r:${r.toRadixString(16)}');
    print('<--R:${R.toRadixString(16)}');
    return R == r;
  }

  Uint8List _hashMessageIfNeeded(Uint8List message) {
    if (_digest != null) {
      _digest!.reset();
      return _digest!.process(message);
    } else {
      return message;
    }
  }

  BigInt _calculateE(BigInt n, Uint8List message) {
    var log2n = n.bitLength;
    var messageBitLength = message.length * 8;

    if (log2n >= messageBitLength) {
      return utils.decodeBigIntWithSign(1, message);
    } else {
      var trunc = utils.decodeBigIntWithSign(1, message);

      trunc = trunc >> (messageBitLength - log2n);

      return trunc;
    }
  }

  ECPoint? _sumOfTwoMultiplies(ECPoint P, BigInt a, ECPoint Q, BigInt b) {
    var c = P.curve;

    if (c != Q.curve) {
      throw ArgumentError('P and Q must be on same curve');
    }

    // Point multiplication for Koblitz curves (using WTNAF) beats Shamir's trick
    // TODO: uncomment this when F2m available
    /*
    if( c is ECCurve.F2m ) {
      ECCurve.F2m f2mCurve = (ECCurve.F2m)c;
      if( f2mCurve.isKoblitz() ) {
        return P.multiply(a).add(Q.multiply(b));
      }
    }
    */

    return _implShamirsTrick(P, a, Q, b);
  }

  ECPoint? _implShamirsTrick(ECPoint P, BigInt k, ECPoint Q, BigInt l) {
    var m = max(k.bitLength, l.bitLength);

    var Z = P + Q;
    var R = P.curve.infinity;

    for (var i = m - 1; i >= 0; --i) {
      R = R!.twice();

      if (_testBit(k, i)) {
        if (_testBit(l, i)) {
          R = R! + Z;
        } else {
          R = R! + P;
        }
      } else {
        if (_testBit(l, i)) {
          R = R! + Q;
        }
      }
    }

    return R;
  }
}

class _RFC6979KCalculator {
  final Mac _mac;
  // ignore: non_constant_identifier_names
  late Uint8List _K;
  // ignore: non_constant_identifier_names
  late Uint8List _V;
  final BigInt _n;

  _RFC6979KCalculator(this._mac, this._n, BigInt d, Uint8List message) {
    _V = Uint8List(_mac.macSize);
    _K = Uint8List(_mac.macSize);
    _init(d, message);
  }

  void _init(BigInt d, Uint8List message) {
    _V.fillRange(0, _V.length, 0x01);
    _K.fillRange(0, _K.length, 0x00);

    var x = Uint8List((_n.bitLength + 7) ~/ 8);
    var dVal = _asUnsignedByteArray(d);

    x.setRange((x.length - dVal.length), x.length, dVal);

    var m = Uint8List((_n.bitLength + 7) ~/ 8);

    var mInt = _bitsToInt(message);

    if (mInt > _n) {
      mInt -= _n;
    }

    var mVal = _asUnsignedByteArray(mInt);

    m.setRange((m.length - mVal.length), m.length, mVal);

    _mac.init(KeyParameter(_K));

    _mac.update(_V, 0, _V.length);
    _mac.updateByte(0x00);
    _mac.update(x, 0, x.length);
    _mac.update(m, 0, m.length);
    _mac.doFinal(_K, 0);

    _mac.init(KeyParameter(_K));
    _mac.update(_V, 0, _V.length);
    _mac.doFinal(_V, 0);

    _mac.update(_V, 0, _V.length);
    _mac.updateByte(0x01);
    _mac.update(x, 0, x.length);
    _mac.update(m, 0, m.length);
    _mac.doFinal(_K, 0);

    _mac.init(KeyParameter(_K));
    _mac.update(_V, 0, _V.length);
    _mac.doFinal(_V, 0);
  }

  BigInt nextK() {
    var t = Uint8List((_n.bitLength + 7) ~/ 8);

    for (;;) {
      var tOff = 0;

      while (tOff < t.length) {
        _mac.update(_V, 0, _V.length);
        _mac.doFinal(_V, 0);

        if ((t.length - tOff) < _V.length) {
          t.setRange(tOff, t.length, _V);
          tOff += (t.length - tOff);
        } else {
          t.setRange(tOff, tOff + _V.length, _V);
          tOff += _V.length;
        }
      }

      var k = _bitsToInt(t);

      // ignore: unrelated_type_equality_checks
      if ((k == 0) || (k >= _n)) {
        _mac.update(_V, 0, _V.length);
        _mac.updateByte(0x00);
        _mac.doFinal(_K, 0);

        _mac.init(KeyParameter(_K));
        _mac.update(_V, 0, _V.length);
        _mac.doFinal(_V, 0);
      } else {
        return k;
      }
    }
  }

  BigInt _bitsToInt(Uint8List t) {
    var v = utils.decodeBigIntWithSign(1, t);
    if ((t.length * 8) > _n.bitLength) {
      v = v >> ((t.length * 8) - _n.bitLength);
    }

    return v;
  }

  Uint8List _asUnsignedByteArray(BigInt value) {
    var bytes = utils.encodeBigInt(value);

    if (bytes[0] == 0) {
      return Uint8List.fromList(bytes.sublist(1));
    } else {
      return Uint8List.fromList(bytes);
    }
  }
}

class _RandomKCalculator {
  final BigInt _n;
  final SecureRandom _random;

  _RandomKCalculator(this._n, this._random);

  BigInt nextK() {
    BigInt k;
    do {
      k = _random.nextBigInteger(_n.bitLength);
    } while (k == BigInt.zero || k >= _n);
    return k;
  }
}
