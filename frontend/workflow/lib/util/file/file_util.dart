import 'dart:io';
import 'dart:typed_data';

class FileLib {
  static Directory tempDir = Directory.systemTemp;
  static Directory appDir = Directory.current;

  static Future<String> readAsString(String path) async {
    File file = File("${tempDir.path}/$path");
    String res = await file.readAsString();
    return res;
  }

  static String readAsStringSync(String path) {
    File file = File("${tempDir.path}/$path");
    String res = file.readAsStringSync();
    return res;
  }

  static Future<Uint8List> readAsBytes(String path) async {
    File file = File("${tempDir.path}/$path");
    Uint8List res = await file.readAsBytes();
    return res;
  }

  static Uint8List readAsBytesSync(String path) {
    File file = File("${tempDir.path}/$path");
    Uint8List res = file.readAsBytesSync();
    return res;
  }

  static Future<File> writeAsString(String path, String contents) async {
    File file = File("${tempDir.path}/$path");
    File newFile = await file.writeAsString(contents);
    return newFile;
  }

  static void writeAsStringSync(String path, String contents) {
    File file = File("${tempDir.path}/$path");
    file.writeAsStringSync(contents);
  }

  static Future<File> writeAsBytes(String path, List<int> bytes) async {
    File file = File("${tempDir.path}/$path");
    File newFile = await file.writeAsBytes(bytes);
    return newFile;
  }

  static void writeAsBytesSync(String path, List<int> bytes) {
    File file = File("${tempDir.path}/$path");
    file.writeAsBytesSync(bytes);
  }
}
