import 'dart:math';

import 'package:flutter/material.dart';

class ColorLib {
  static Color randomColor({
    int limitA = 120,
    int limitR = 0,
    int limitG = 0,
    int limitB = 0,
  }) {
    Random random = Random();
    int a = limitA + random.nextInt(256 - limitA); //透明度值
    int r = limitR + random.nextInt(256 - limitR); //红值
    int g = limitG + random.nextInt(256 - limitG); //绿值
    int b = limitB + random.nextInt(256 - limitB); //蓝值
    return Color.fromARGB(a, r, g, b); //生成argb模式的颜色
  }

  /// 使用方法:
  /// var color1=ColorLib.parse("#33428A43");
  /// var color2=ColorLib.parse("33428A43");
  /// var color3=ColorLib.parse("#428A43");
  /// var color4=ColorLib.parse("428A43");
  /// var color5=ColorLib.parse("255,128,80");
  /// var color6=ColorLib.parse("120,255,128,80");

  static List<Color>? fromHexStr(String code) {
    if (code.length % 8 != 0) return null;
    List<Color> result = [];
    for (int i = 0; i < code.length; i += 8) {
      result.add(ColorLib.parse(code.substring(i, i + 8)) ?? Colors.black);
    }
    return result;
  }

  static Color? parse(String? code) {
    Color? result;
    int value = 0;
    if (code == null) {
      return null;
    }
    List<String> part = code.split(",");
    if (part.length == 3 || part.length == 4) {
      if (part.length == 3) {
        //不带透明度,例如 (255,255,0)
        try {
          Map<int, int> partMap =
              part.reversed.map((a) => int.parse(a)).toList().asMap();
          value = partMap.keys
              .map<int>((i) => (pow(256, i) * partMap[i]!).toInt())
              .reduce((a, b) => a + b);
          result = Color(value + 0xFF000000);
        } catch (e) {
          return null;
        }
      } else {
        //带透明度，例如(128,255,0,0) argb格式
        try {
          var part0 = part[0];
          part = part.sublist(1).reversed.toList()..add(part0); //透明度在最高位
          Map<int, int> partMap =
              part.map((a) => int.parse(a)).toList().asMap();
          value = partMap.keys
              .map<int>((i) => (pow(256, i) * partMap[i]!).toInt())
              .reduce((a, b) => a + b);
          result = Color(value);
        } catch (e) {
          return null;
        }
      }
    } else {
      if (code.contains("#")) {
        try {
          value = int.parse(code.substring(1), radix: 16);
        } catch (e) {
          return null;
        }
        switch (code.length) {
          case const (1 + 6): //6位
            result = Color(value + 0xFF000000);
            break;
          case const (1 + 8): //8位
            result = Color(value);
            break;
          default:
            result = null;
        }
      } else {
        try {
          value = int.parse(code, radix: 16);
        } catch (e) {
          return null;
        }
        switch (code.length) {
          case 6:
            result = Color(value + 0xFF000000);
            break;
          case 8:
            result = Color(value);
            break;
          default:
            result = null;
        }
      }
    }
    return result;
  }

  static String? colorString(Color? color) => color == null
      ? null
      : "#${color.value.toRadixString(16).padLeft(8, '0').toUpperCase()}";
}
