import 'dart:convert';

import 'package:yaml/yaml.dart';
import 'package:yaml_writer/yaml_writer.dart';

class StringLib {
  ///
  static List<String?> regMatch1(String s, RegExp r) {
    RegExpMatch? a = r.firstMatch(s);
    List<String?> c = [];
    if (a == null) return [];
    for (int i = 0; i <= a.groupCount; i++) {
      c.add(a.group(i));
    }
    return (c);
  }

  static String getBlobsId(String account) {
    String preAccount = account.substring(0, 8);
    String idString = "${BigInt.parse(preAccount, radix: 16)}";
    int edgesCount = int.parse(idString.substring(0, 2)) % 17 + 3; //3-20
    int minGrowth = int.parse(idString.substring(2, 3)) % 7 + 2; //2-9
    int random = int.parse(idString.substring(3)) % 65535 + 1;

    return "$edgesCount-$minGrowth-$random";
  }

  /// 返回部分掩码的字符串
  /// - head 头部保留的长度,默认4
  /// - tail 尾部保留的长度,默认4
  /// - maskChar 掩码的字符,默认'.'
  /// - maskLen  掩码的长度,默认3
  /// - sample:
  /// ```dart
  ///   mask("abcde12345") ==> abcd...2345
  ///   mask("abcde12345",head:2,tail:3) ==> ab...345
  ///   mask("abcde12345",maskChar:'*',maskLen:2) ==> abcd**2345
  ///   mask("abcde12345",head:0,tail:1,maskLen:10) ==> ..........5
  /// ```
  static String mask(String str,
      {int head = 4, int tail = 4, String maskChar = '.', int maskLen = 3}) {
    late String out;
    int len = str.length;
    if (len > head + tail) {
      final headStr = str.substring(0, head);
      final tailStr = str.substring(len - tail);
      String maskStr = "";
      List.generate(maskLen, (idx) => maskStr += maskChar);
      out = headStr + maskStr + tailStr;
    } else {
      out = str;
    }
    return out;
  }

  static List<String?> regMatch(String s, RegExp r) {
    List x = [];
    r.allMatches(s).forEach((item) {
      x.add(item.groups(List.generate(item.groupCount, (i) => i)));
    });
    List k = x.map((o) => o.where((p) => p != null).length).toList();
    if (k.isEmpty) return []; //如果没有任何匹配
    int q = k.reduce((o, p) => o > p ? o : p);
    return x[k.indexOf(q)];
  }

  /// 将任何对象的String用定义的转换函数替换
  /// ### 1、定义的转换函数，带入replaceString。例如:
  /// ```dart
  /// String fun(String str){
  ///   if (str=="sos"){
  ///     return "*$str*";
  ///   }
  ///   return str;
  /// }
  ///
  /// var a=[1,"sos",3,["sos",2,[3,"c",{"k":[{"x":"sos"},{"y":"tot"}]}]]];
  /// print(StringLib.replaceString(a,fun));
  /// //a is [1, *sos*, 3, [*sos*, 2, [3, c, {k: [{x: *sos*}, {y: tot}]}]]]
  ///
  /// ````
  /// ### 2、有时需要确保原参数不变，使用json的两次转换来完成深拷贝。例如：
  /// ```dart
  /// import 'dart:convert';
  /// var a=[1,"sos",3,["sos",2,[3,"c",{"k":[{"x":"sos"},{"y":"tot"}]}]]];
  /// var b=a; // StringLib.replaceString(b,fun) 将会改变a和b
  /// var c=json.decode(json.encode(a));
  /// StringLib.replaceString(c,fun) 将不会影响a
  /// ```
  /// ###  3、其他例子
  /// ```dart
  /// StringLib.replaceString(123,fun); //123
  /// StringLib.replaceString(["a","sos","b"],fun); //["a","*sos","b"]
  /// StringLib.replaceString("sos",fun); //"*sos*"
  /// StringLib.replaceString({"a":"sos","b":123,"c":"so"},fun); //{"a":"*sos*","b":123,"c":"so"}
  /// ```
  static T? replaceString<T>(T obj, [Function(String)? func]) {
    if (obj is List) {
      int d = 0;
      obj.forEach((i) {
        if (i is List || i is Map<String, dynamic>) {
          replaceString(i, func);
        } else if (i is String) {
          //print("1:$i");
          if (func != null) obj[d] = func(i);
        }
        d++;
      });
      return obj;
    } else if (obj is Map<String, dynamic>) {
      obj.keys.forEach((j) {
        if (obj[j] is Map || obj[j] is List) {
          replaceString(obj[j], func);
        } else if (obj[j] is String) {
          //print("2:${obj[j]}");
          if (func != null) obj[j] = func(obj[j]);
        }
      });
      return obj;
    } else if (obj is String) {
      //print("3:$obj");
      if (func != null) return (func(obj));
      return obj;
    } else {
      return obj;
    }
  }

  static json2yamlStr(Map<String, dynamic> obj) {
    return YAMLWriter().write(obj);
  }

  static Map<String, dynamic> yamlStr2json(String yamlStr) {
    YamlNode yamlNode = loadYaml(yamlStr);
    Map<String, dynamic> result = {};
    StringLib.yaml2json(yamlNode, result);
    return json.decode(json.encode(result));
  }

  static yaml2json(YamlNode node, Object current) {
    switch (node.runtimeType) {
      case YamlMap:
        (node as YamlMap).keys.forEach((item) {
          String key = "$item";
          //print(node[item].runtimeType);
          switch (node[item].runtimeType) {
            case int:
            case double:
            case String:
            case bool:
            case Null:
              (current as Map)[key] = node[item];
              break;
            case YamlMap:
              (current as Map)[key] = {};
              yaml2json(node[item], current[key]);
              break;
            case YamlList:
              (current as Map)[key] = [];
              yaml2json(node[item], current[key]);
              break;
            default:
              print("*:${node[item].runtimeType}");
          }
        });
        break;
      case YamlList:
        node.value.forEach((item) {
          switch (item.runtimeType) {
            case int:
            case double:
            case String:
            case bool:
            case Null:
              (current as List).add(item);
              break;
            case YamlMap:
              Map<String, dynamic> newNode = {};
              (current as List).add(newNode);
              yaml2json(item, newNode);
              break;
            case YamlList:
              List newNode = [];
              (current as List).add(newNode);
              yaml2json(item, newNode);
              break;
            default:
              print("@:${item.runtimeType}");
          }
        });
        break;
      default:
        print("??:${node.runtimeType}");
    }
  }
}
