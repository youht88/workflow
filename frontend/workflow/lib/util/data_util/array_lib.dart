import 'math_lib.dart';

class ArrayLib {
  static T? pickRandom<T>(List<T> list) {
    int len = list.length;
    if (len == 0) return null;
    return list[MathLib.random.nextInt(len)];
  }

  static T? decode<W, T>(W x, List<W> a, List<T> b) {
    assert(a.length == b.length);
    int idx = a.indexWhere((item) => item == x);
    if (idx == -1) {
      return null;
    } else {
      return b[idx];
    }
  }
}
