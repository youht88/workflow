import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:workflow/data/constant.dart';
import 'package:workflow/util/network/http_util.dart';

/// * updateFile的例子
/// ```dart
/// Future<void> add() async {
///   ID.value = "";
///   Hash.value = "";
///   if (imagePath.value != "") {
///     final res = await icfs.add(imagePath.value, onSendProgress: (a, b) {
///       c1.change(a / b * 0.9);
///     }, onRecieveProgress: (a, b) {
///       c1.change(1);
///     });
///     Hash.value = res["Hash"];
///   }
///   final res1 = await icfs.id();
///   ID.value = res1["ID"];
/// }
/// ```
///

/// ICFS 封装icfs http api
///
class ICFS {
  final String baseUrl;
  final String basePath = "/api/v0";
  late String base;
  late String url;
  Map<String, dynamic>? headers;
  ICFS({this.baseUrl = Constant.baseIcfsApiUrl, this.headers}) {
    base = baseUrl + basePath;
  }

  Future<dynamic> id() async {
    final url = "$base/id";
    final response = await HttpLib.postData(url, headers: headers);
    return response;
  }

  Future<dynamic> swarmPeers({bool? streams, bool? latency}) async {
    final url = "$base/swarm/peers";
    final response = await HttpLib.postData(url,
        queryParameters: {
          "streams": streams ?? true,
          "latency": latency ?? true,
        },
        headers: headers);
    return response;
  }

  Future<dynamic> dagGet(String cid) async {
    final url = "$base/dag/get?arg=$cid";
    final response = await HttpLib.postData(url, headers: headers);
    return response;
  }

  Future<dynamic> dagPut(
    File file, {
    bool pin = true,
    dynamic Function(int, int)? onSendProgress,
    dynamic Function(int, int)? onRecieveProgress,
  }) async {
    final url = "$base/dag/put";
    final response = await HttpLib.uploadFile(url, file,
        body: {"pin": pin}, headers: headers);
    return response;
  }

  Future<dynamic> cat(String cid) async {
    final url = "$base/cat?arg=$cid";
    final response = await HttpLib.postData(url, headers: headers);
    return response;
  }

  Future<dynamic> get(String cid) async {
    final url = "$base/get?arg=$cid";
    final response = await HttpLib.postData(url,
        responseType: ResponseType.bytes, headers: headers);
    return response;
  }

  Future<dynamic> ls(String cid) async {
    final url = "$base/ls?arg=$cid";
    final response = await HttpLib.postData(url, headers: headers);
    return response;
  }

  Future<dynamic> add(File file,
      {bool recursive = false,
      bool quieter = true,
      bool progress = false,
      bool pin = true,
      dynamic Function(int, int)? onSendProgress,
      dynamic Function(int, int)? onRecieveProgress}) async {
    final url = "$base/add";
    //debugPrint(headers?['Authorization']);
    Map<String, dynamic> h = Map<String, dynamic>.from(headers ?? {});
    h["Transfer-Encoding"] = "chunked";
    final response = await HttpLib.uploadFile(
      url,
      file,
      headers: h,
      // body: {"pin": pin, "quieter": quieter, "progress": progress},
      onSendProgress: onSendProgress,
      onReceiveProgress: onRecieveProgress,
    );
    //debugPrint('-------------------------');
    //debugPrint(response);
    return response;
  }

  /// lifttime 发布生效后的持续时间.格式例如 “300s”, “1.5h” or “2h45m”, 时间单位: “ns”, “us” (or “µs”), “ms”, “s”, “m”, “h”. 默认为: 24h
  /// key [string]: “ipfs key list -l” 指令列出的使用键或者一个有效的 PeerID 的名称, 默认是: self
  Future<dynamic> namePublish(String cid,
      {String lifetime = "24h", String key = "self"}) async {
    final url = "$base/name/publish?arg=$cid&lifetime=$lifetime&key=$key";
    final response = await HttpLib.postData(url, headers: headers);
    return response;
  }

  /// 生成name对应的key，默认算法256位ed22519
  Future<dynamic> keyGen(String name,
      {String type = "ed25519", int size = 256}) async {
    final url = "$base/key/gen?arg=$name&type=$type&size=$size";
    final response = await HttpLib.postData(url, headers: headers);
    return response;
  }

  Future<dynamic> keyList() async {
    final url = "$base/key/list";
    final response = await HttpLib.postData(url, headers: headers);
    return response;
  }

  Future<dynamic> pubsubPub(String topic, String data) async {
    final url = "$base/pubsub/pub?arg=$topic&arg=$data";
    debugPrint("$headers");
    final response = await HttpLib.postData(url, headers: headers);
    return response;
  }

  Future<void> pubsubSub(String topic,
      {required Function(Map) onReceiveData, CancelToken? cancelToken}) async {
    final url = "$base/pubsub/sub?arg=$topic";
    http.Client client = http.Client();
    debugPrint(baseUrl);
    http.BaseRequest req = http.Request(
        "POST",
        Uri.http(
          "${Uri.parse(baseUrl).host}:${Uri.parse(baseUrl).port}",
          "/api/v0/pubsub/sub",
          {"arg": topic},
        ));

    req.headers["Authorization"] = (headers?["Authorization"]) ?? "";
    debugPrint("${req.headers}");
    Future<http.StreamedResponse> res = client.send(req);

    res.asStream().listen((streamedResponse) {
      streamedResponse.stream.listen((x) {
        var data = json.decode(utf8.decode(x));
        onReceiveData(data);
        String message = utf8.decode(base64.decode(data["data"]));
        debugPrint("Recived data:$message");
      });
    });
    return;
  }
}

class ResponseInterceptor extends Interceptor {
  @override
  void onResponse(
    Response response,
    ResponseInterceptorHandler handler,
  ) {
    debugPrint("response:$response,handler:$handler");
  }
}
