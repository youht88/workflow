import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:workflow/util/data_util/data_util.dart';

/// * 简单封装dio的get、post和updateFile。
/// get、post默认返回json类型，如果需要返回的bytes类型（比如图片、声音、食品、pdf文件等）
/// 需要设置 responseType: ResponseType.bytes
/// ```dart
/// Map<String,dynamic> a = await HttpClient.getData("http://www.api.com");
/// Map<String,dynamic> b = awati HttpClient.postData("http://www.post.com",body:{"param1":value1});
/// Uint8List c = await HttpClient.getData("http://www.img.com/img.jpg",responseType:ResponseType.bytes);
/// ```
///
class HttpLib {
  static const utf8Decoder = Utf8Decoder();

  static Future<dynamic> getDataRaw(String url,
      {Map<String, dynamic>? queryParameters}) async {
    return getData(
      url,
      queryParameters: queryParameters,
      responseType: ResponseType.bytes,
    );
  }

  static Future<dynamic> getData(String url,
      {ResponseType responseType = ResponseType.json,
      Map<String, dynamic>? headers,
      Map<String, dynamic>? queryParameters}) async {
    Response? response;
    final res;
    debugPrint("Get-URL:$url");
    debugPrint("Get-Body:${JsonLib.convert(queryParameters)}");
    response =
        await Dio(BaseOptions(responseType: responseType, headers: headers))
            .get(url, queryParameters: queryParameters);
    debugPrint("Get-Response:${JsonLib.convert(response)}");
    if (response.statusCode == 200) {
      //res = json.decode(response.data.toString());
      res = response.data;
      return res;
    }
    String msg = '网络异常,${response.statusMessage}';
    throw msg;
    //throw msg;
  }

  //为什么会报405错误？？？
  static Future<dynamic> postDataRaw(String url,
      {Map<String, dynamic>? queryParameters,
      Map<String, dynamic>? headers,
      Map<String, dynamic>? data,
      Function(int, int)? onSendProgress,
      Function(int, int)? onReceiveProgress}) async {
    dynamic res = await postData(url,
        queryParameters: queryParameters,
        headers: headers,
        data: data,
        responseType: ResponseType.bytes,
        onSendProgress: onSendProgress,
        onReceiveProgress: onReceiveProgress);
    return res;
  }

  static Future postData(String url,
      {Map<String, dynamic>? queryParameters,
      dynamic data,
      CancelToken? cancelToken,
      ResponseType responseType = ResponseType.json,
      Map<String, dynamic>? headers,
      Function(int, int)? onSendProgress,
      Function(int, int)? onReceiveProgress}) async {
    Response? response;
    final res;
    var dio = Dio(BaseOptions(responseType: responseType, headers: headers));
    //dio.interceptors.add(ResponseInterceptor());
    debugPrint("Post-URL:$url");
    debugPrint("Post-Body:${JsonLib.convert(data)}");
    response = await dio.post(url,
        data: data,
        cancelToken: cancelToken,
        queryParameters: queryParameters,
        onSendProgress: onSendProgress,
        onReceiveProgress: onReceiveProgress);
    debugPrint("Post-Response:${JsonLib.convert(response)}");
    if (response.statusCode.toString().startsWith('2')) {
      res = response.data;
      return res;
    }
    String msg = '网络异常[${response.statusCode}],${response.statusMessage}';
    throw msg;
  }

  static Future postDataV2(String url,
      {Map<String, dynamic>? queryParameters,
      dynamic data,
      CancelToken? cancelToken,
      ResponseType responseType = ResponseType.json,
      Map<String, dynamic>? headers,
      Function(int, int)? onSendProgress,
      Function(int, int)? onReceiveProgress}) async {
    Response? response;
    final res;
    var dio = Dio(BaseOptions(responseType: responseType, headers: headers));
    //dio.interceptors.add(ResponseInterceptor());
    response = await dio.post(url,
        data: data,
        cancelToken: cancelToken,
        queryParameters: queryParameters,
        onSendProgress: onSendProgress,
        onReceiveProgress: onReceiveProgress);
    if (response.data['statusCode'] == 200) {
      res = response.data;
      return res;
    } else {
      return null;
    }
  }

  static uploadFile(String url, File file,
      {Map<String, dynamic>? body,
      ResponseType responseType = ResponseType.json,
      Map<String, dynamic>? headers,
      Function(int, int)? onSendProgress,
      Function(int, int)? onReceiveProgress}) async {
    String path = file.path;
    final res;
    var name = path.substring(path.lastIndexOf("/") + 1, path.length);
    var suffix = name.substring(name.lastIndexOf(".") + 1, name.length);
    FormData formData = FormData.fromMap(
        {"file": await MultipartFile.fromFile(path, filename: name)});

    var response =
        await Dio(BaseOptions(responseType: responseType, headers: headers))
            .post<String>(url,
                data: formData,
                queryParameters: body,
                onSendProgress: onSendProgress,
                onReceiveProgress: onReceiveProgress);
    if (response.statusCode == 200) {
      // form 形式默认返回 response.data是String类型
      res = json.decode(response.data ?? "");
      return res;
    }
    String msg = '网络异常,${response.statusMessage}';
    throw msg;
  }
}
