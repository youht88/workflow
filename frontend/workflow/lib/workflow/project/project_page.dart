import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:uuid/uuid.dart';
import 'package:workflow/workflow/project/project_controller.dart';

class ProjectWidget extends StatelessWidget {
  String projectName;
  String? projectId;
  ProjectController projectController = Get.find<ProjectController>();
  ProjectWidget(
      {super.key, required this.projectName, this.projectId, required mode}) {
    projectController.projectName = projectName;
    projectController.projectId = projectId ?? const Uuid().v4();
    projectController.widget = this;
    projectController.mode = mode;
  }

  @override
  Widget build(BuildContext context) {
    debugPrint("!!!!!!!");
    return GetBuilder(
        init: projectController,
        global: false,
        builder: (_) {
          return Stack(children: projectController.works);
        });
  }
}
