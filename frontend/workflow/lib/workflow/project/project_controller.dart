import 'dart:convert';
import 'dart:io';

import 'package:event_bus/event_bus.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:json_dynamic_widget/json_dynamic_widget.dart';
import 'package:path_provider/path_provider.dart';
import 'package:workflow/util/js/js_util.dart';
import 'package:workflow/workflow/chat/chat_controller.dart';
import 'package:workflow/workflow/flow/flow_controller.dart';
import 'package:workflow/workflow/project/project_page.dart';
import 'package:workflow/workflow/work/work_design_page.dart';
import 'package:workflow/workflow/work/work_page.dart';

import '../work/work3_page.dart';
import '../work/work_chat_page.dart';

class ProjectController extends GetxController {
  //late WorkflowController workflowController;
  late ProjectWidget widget;
  late String projectName;
  late String projectId;
  List<WorkWidget> works = [];
  FlowController? currentFlowController;
  late JsonWidgetRegistry projectJsonWidgetRegistry;
  JsLib jsLib = JsLib();
  EventBus eventBus = EventBus();
  //模式可以是设计design或者部署运行deploy,默认是design模式
  String mode = 'design';
  @override
  onInit() {
    super.onInit();
    projectJsonWidgetRegistry = JsonWidgetRegistry(
        overrideInternalFunctions: false,
        functions: {
          ...JsonWidgetInternalFunctionsBuilder().withSetValue().build(),
          ...<String, JsonWidgetFunction>{
            'customFunction': ({args, required registry}) {
              debugPrint("This is a custom registry function.");
              return 'hello word!!';
            },
            'add': ({args, required registry}) {
              jsLib.setVar("args", args);
              return jsLib.eval("""
                 x = getVar('args')[0];
                 x['a']+x['b']
             """);
            }
          }
        },
        parent: JsonWidgetRegistry.instance);
  }

  WorkWidget addWorkWidget(String type, [String? name]) {
    WorkWidget item;
    switch (type) {
      case 'work_design':
        item = WorkDesignWidget(
            projectController: this,
            //id: "a",
            //targetIds: const ["ID2", "ID3"],
            name: name);
        works.add(item);
        break;
      case 'work_chat':
        item = WorkChatWidget(
          projectController: this,
        );

        Get.put(ChatController());
        ChatController chatController = Get.find<ChatController>();
        chatController.addAiChat(item);
        break;
      case 'work3':
      default:
        item = Work3Widget(
          projectController: this,
        );
        works.add(item);
    }
    update();

    return item;
  }

  export() async {
    Directory appDocDir = await getApplicationDocumentsDirectory();
    String appDocPath = appDocDir.path;
    debugPrint(appDocPath);
    try {
      //选择文件夹
      String? folderPath = await FilePicker.platform.getDirectoryPath();

      // 创建一个新的目录
      Directory newDirectory = Directory('$folderPath/$projectName');
      await newDirectory.create(recursive: true);

      int index = 0;
      Map<String, dynamic> totalContent = {};
      for (WorkWidget item in works) {
        index++;
        String workName = "work$index";
        Map<String, dynamic> content = item.workController.export();
        totalContent[workName] = content;
      }
      // 在新目录中创建一个新的文件
      File file = File('${newDirectory.path}/work.json');

      // 写入文件内容
      await file.writeAsString(
          const JsonEncoder.withIndent('    ').convert(totalContent));
    } catch (e) {
      debugPrint("$e");
    }
  }

  import() async {
    Directory appDocDir = await getApplicationDocumentsDirectory();
    String appDocPath = appDocDir.path;
    try {
      //选择文件夹
      String? folderPath = await FilePicker.platform.getDirectoryPath();
      if (folderPath == null) return;
      debugPrint(folderPath);
      // 创建一个文件对象
      File file = File('$folderPath/work.json');
      // 读取文件内容
      String fileContent = await file.readAsString();
      debugPrint("===>$fileContent");
      Map<String, dynamic> fileMap = json.decode(fileContent);
      for (var item in fileMap.entries) {
        WorkWidget workWidget = addWorkWidget('work1', item.key);
        workWidget.workController.import(item.value);
      }
    } catch (e) {
      debugPrint("$e");
    }
  }
}
