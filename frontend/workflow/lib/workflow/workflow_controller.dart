import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:logging/logging.dart';
import 'package:tabbed_view/tabbed_view.dart';
import 'package:workflow/workflow/element/element_controller.dart';
import 'package:workflow/workflow/flow/flow_controller.dart';
import 'package:workflow/workflow/project/project_controller.dart';
import 'package:workflow/workflow/project/project_page.dart';
import 'package:workflow/workflow/work/work_page.dart';

import 'grid_background.dart';
import 'work/work.dart';

class WorkflowController extends GetxController {
  late TabbedViewController projectTabController;
  List<TabData> projectTabs = [];
  List<ProjectWidget> projects = [];
  ProjectWidget? currentProjectWidget;
  @override
  onInit() {
    super.onInit();
    Get.create(() => ProjectController());
    Get.create(() => WorkController());
    Get.create(() => FlowController());
    Get.create(() => ElementController());
    projectTabController =
        TabbedViewController(projectTabs, reorderEnable: false);
    Logger.root.level = Level.FINEST;
    Logger.root.onRecord.listen((record) {
      if (record.loggerName == 'log') {
        debugPrint(
            '${record.level.name}: ${record.loggerName}: ${record.time}: ${record.message}');
      }
    });
  }

  @override
  onReady() {
    addProjectWidget('project1');
  }

  List<String> getProjectNames() {
    return projects.map((item) => item.projectController.projectName).toList();
  }

  List<List<WorkWidget>> getProjectWorkWidgets() {
    return projects.map((item) => item.projectController.works).toList();
  }

  ProjectWidget addProjectWidget(projectName, [projectId]) {
    ProjectWidget projectWidget = ProjectWidget(
        projectName: projectName, projectId: projectId, mode: 'design');
    projects.add(projectWidget);
    projectTabController.addTab(TabData(
        text: projectName,
        // leading: (context, status) {
        //   return InkWell(
        //     onTap: () {
        //       StarMenuOverlay.displayStarMenu(
        //         context,
        //         StarMenu(
        //             parentContext: context,
        //             params: getMenuParams(
        //               context,
        //               onHoverScale: 1,
        //               centerOffset: const Offset(150, 150),
        //             ),
        //             items: [Text(projectName ?? "")]),
        //       );
        //     },
        //     child: const Icon(Icons.note_alt, size: 16),
        //   );
        // },
        content: Stack(children: [
          const Positioned.fill(
            child: GridBackground(params: GridBackgroundParams()),
          ),
          projectWidget
        ])));
    currentProjectWidget = projectWidget;
    update();
    return projectWidget;
  }

  ProjectWidget? selectProjectWidget(int index) {
    currentProjectWidget = projects[index];
    update();
    return currentProjectWidget;
  }

  removeProjectWidget(int index) {
    projects.removeAt(index);
    if (projects.isNotEmpty) {
      currentProjectWidget = selectProjectWidget(0);
    }
    update();
  }
}
