import 'package:flutter/material.dart';
import 'package:star_menu/star_menu.dart';

StarMenuParameters getMenuParams(BuildContext context,
    {Offset? centerOffset, double onHoverScale = 1.1}) {
  return StarMenuParameters(
    shape: MenuShape.linear,
    openDurationMs: 60,
    linearShapeParams: const LinearShapeParams(
      angle: 270,
      alignment: LinearAlignment.left,
      space: 10,
    ),
    onHoverScale: onHoverScale,
    centerOffset: centerOffset ??
        Offset.zero, //const Offset(30, 30), //position + const Offset(10, 0),
    backgroundParams: const BackgroundParams(
      backgroundColor: Colors.transparent,
    ),
    boundaryBackground: BoundaryBackground(
      padding: const EdgeInsets.all(16),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        color: Theme.of(context).cardColor,
        boxShadow: kElevationToShadow[6],
      ),
    ),
  );
}
