import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:workflow/workflow/element/element_controller.dart';
import 'package:workflow/workflow/tools/color_picker_widget.dart';
import 'package:workflow/workflow/tools/input_widget.dart';
import 'package:workflow/workflow/tools/multi_slider_widget.dart';
import 'package:workflow/workflow/work/work_controller.dart';

displayWorkMenu(BuildContext context, Offset position,
    WorkController workController, ElementController? elementController) {
  debugPrint("$position");

  // StarMenuOverlay.displayStarMenu(
  //   context,
  //   StarMenu(
  //     params: getMenuParams(context),
  //     onItemTapped: (index, controller) {
  //       if (index == 0 || index >= 4) {
  //         controller.closeMenu!();
  //       }
  //     },
  //     items: [
  //
  Get.defaultDialog(
      title: "设置work",
      titleStyle: const TextStyle(fontSize: 18),
      content: SizedBox(
        width: 300,
        height: 350,
        child: ListView(children: [
          InkWell(
            onTap: () {},
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Text("名称:",
                    style: TextStyle(fontWeight: FontWeight.bold)),
                Text(workController.name,
                    style: const TextStyle(
                        fontWeight: FontWeight.bold, fontSize: 10))
              ],
            ),
          ),
          const SizedBox(height: 8),
          InputWidget(
              titleText: "提示词:",
              initText: workController.helloMessage,
              hintText: "有什么可以帮你的?",
              onChanged: (value) {
                workController.setHelloMessage(value);
              }),
          GetBuilder(
              init: workController,
              global: false,
              builder: (context) {
                return ColorPickerWidget(
                  initColor: workController.bgColor,
                  width: 220,
                  height: 110,
                  titleWidget: Row(
                    children: [
                      const Text("更改背景颜色"),
                      const SizedBox(width: 10),
                      Row(
                        children: [
                          Checkbox(
                              value: workController.isTransparent,
                              onChanged: (value) {
                                if (value!) {
                                  workController.setBgColor(Colors.transparent);
                                }
                              }),
                          const Text("透明"),
                        ],
                      )
                    ],
                  ),
                  onColorChange: (Color value) {
                    workController.isTransparent = false;
                    workController.setBgColor(value);
                  },
                );
              }),
          GetBuilder<WorkController>(
            init: workController,
            global: false,
            builder: (_) {
              return MultiSliderWidget(
                width: 220,
                height: 120,
                titleWidget: Text(
                    "更改尺寸: ${workController.width.toInt()}  ✖️  ${workController.height.toInt()}"),
                labels: const ['宽度:', '高度:'],
                labelsTextStyle: const [
                  TextStyle(fontSize: 10, color: Colors.black45),
                  TextStyle(fontSize: 10, color: Colors.black45)
                ],
                sliderMin: const [200, 150],
                sliderMax: const [600, 500],
                sliderValue: [workController.width, workController.height],
                sliderOnChanged: [
                  (value) {
                    workController.setWidth(value);
                    workController.flowController.setWidth(value);
                  },
                  (value) {
                    workController.setHeight(value);
                    workController.flowController.setHeight(value);
                  }
                ],
              );
            },
          ),
          const SizedBox(width: 200, child: Divider()),
          InkWell(
            onTap: () {
              Get.back();
              workController.preview(context);
            },
            child: const Row(
                mainAxisSize: MainAxisSize.min,
                children: [Icon(Icons.preview), Text('预览')]),
          ),
          const SizedBox(height: 8),
          InkWell(
            onTap: () {
              Get.back();
              workController.export();
            },
            child: const Row(
                mainAxisSize: MainAxisSize.min,
                children: [Icon(Icons.preview), Text('导出')]),
          ),
          const SizedBox(height: 8),
          InkWell(
            onTap: () {
              Get.back();
              workController.removeWork();
            },
            child: const Row(mainAxisSize: MainAxisSize.min, children: [
              Icon(Icons.delete, color: Colors.red),
              Text('删除Work')
            ]),
          )
        ]),
      ));
}
