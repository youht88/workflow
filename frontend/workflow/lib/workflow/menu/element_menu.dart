import 'package:flutter/material.dart';
import 'package:json_theme/json_theme.dart';
import 'package:star_menu/star_menu.dart';
import 'package:workflow/workflow/element/element_controller.dart';
import 'package:workflow/workflow/menu/menu_params.dart';
import 'package:workflow/workflow/work/work_controller.dart';

displayElementMenu(BuildContext context, Offset position,
    WorkController workController, ElementController? elementController) {
  debugPrint("$position");
  StarMenuOverlay.displayStarMenu(
    context,
    StarMenu(
      params: getMenuParams(context),
      onItemTapped: (index, controller) {
        controller.closeMenu!();
      },
      items: [
        if (elementController == null ||
            elementController.layoutType != null) ...[
          InkWell(
            onTap: () {
              workController.addElement(
                  type: "text", elementController: elementController);
            },
            child: const Row(
                mainAxisSize: MainAxisSize.min,
                children: [Icon(Icons.text_fields), Text("增加Text")]),
          ),
          InkWell(
            onTap: () {
              workController.addElement(
                  type: "icon", elementController: elementController);
            },
            child: const Row(
                mainAxisSize: MainAxisSize.min,
                children: [Icon(Icons.ac_unit), Text("增加Icon")]),
          ),
          InkWell(
            onTap: () {
              workController.addElement(
                  type: "network_image", elementController: elementController);
            },
            child: const Row(
                mainAxisSize: MainAxisSize.min,
                children: [Icon(Icons.image), Text("增加NetworkImage")]),
          ),
          InkWell(
            onTap: () {
              workController.addElement(
                  type: "elevated_button",
                  elementController: elementController);
            },
            child: const Row(
                mainAxisSize: MainAxisSize.min,
                children: [Icon(Icons.abc), Text("增加Button")]),
          ),
          InkWell(
            onTap: () {
              workController.addElement(
                  type: "custom", elementController: elementController);
              debugPrint("${ThemeEncoder.encodeBoxDecoration(BoxDecoration(
                border: Border.all(),
                shape: BoxShape.circle,
                image: const DecorationImage(
                    image: NetworkImage("aaa"), fit: BoxFit.cover),
              ))}");
            },
            child: const Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text("增加Custom"),
              ],
            ),
          ),
          InkWell(
            onTap: () {
              workController.addElement(
                  type: "container", elementController: elementController);
            },
            child: const Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Icon(Icons.add_box_outlined),
                Text("增加Container"),
              ],
            ),
          ),
          InkWell(
            onTap: () {
              workController.addElement(
                  type: "expanded", elementController: elementController);
            },
            child: const Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Icon(Icons.expand),
                Text("增加Expanded"),
              ],
            ),
          ),
          InkWell(
            onTap: () {
              workController.addElement(
                  type: "listview", elementController: elementController);
            },
            child: const Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Icon(Icons.list),
                Text("增加Listview"),
              ],
            ),
          ),
          InkWell(
            onTap: () {
              workController.addElement(
                  type: "row", elementController: elementController);
            },
            child: const Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Icon(Icons.arrow_forward),
                Text("增加Row"),
              ],
            ),
          ),
          InkWell(
            onTap: () {
              workController.addElement(
                  type: "column", elementController: elementController);
            },
            child: const Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Icon(Icons.arrow_downward),
                Text("增加Column"),
              ],
            ),
          ),
          if (elementController != null)
            const SizedBox(width: 100, child: Divider()),
        ],
        if (elementController != null)
          InkWell(
            onTap: () {
              workController.removeElement(elementController);
            },
            child: const Row(mainAxisSize: MainAxisSize.min, children: [
              Icon(Icons.delete),
              Text('删除', style: TextStyle(color: Colors.red))
            ]),
          )
      ],
      parentContext: context,
    ),
  );
}
