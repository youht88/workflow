part of workflow;

//是workflow_page.dart的抽离出来的菜单元素
List<Widget> _menuItems(WorkflowController controller, BuildContext context,
    StarMenuController parentMenuController) {
  StarMenuController testMenuController = StarMenuController();
  StarMenuController chatMenuController = StarMenuController();
  return [
    InkWell(
        onTap: () {
          String projectName = 'project${controller.projects.length + 1}';
          controller.addProjectWidget(projectName);
          //controller.addWorkWidget(projectName);
        },
        child: const Text("新建工程...")),
    InkWell(
        onTap: () {
          controller.currentProjectWidget?.projectController
              .addWorkWidget('work_design');
        },
        child: const Text("增加work design")),
    InkWell(
        onTap: () {
          controller.currentProjectWidget?.projectController
              .addWorkWidget('work_chat');
        },
        child: const Text("增加work chat")),
    InkWell(onTap: () {}, child: const Text("Markdown")).addStarMenu(
        items: [
          InkWell(
              child: const Text("增加work3"),
              onTap: () {
                controller.currentProjectWidget?.projectController
                    .addWorkWidget('work3');
              }),
          const InkWell(child: Text("world"))
        ],
        params: getMenuParams(context, centerOffset: const Offset(-80, 0)),
        controller: testMenuController,
        onItemTapped: (index, menuController) {
          parentMenuController.closeMenu!();
          menuController.closeMenu!();
          debugPrint("${testMenuController == menuController}");
        }),
    InkWell(
        onTap: () {
          debugPrint(
              "${controller.currentProjectWidget?.projectController.works.map((WorkWidget item) => item.workController.id).toList()}");
        },
        child: const Text("按钮")),
    InkWell(
        onTap: () async {
          WorkflowController workflowController =
              Get.find<WorkflowController>();
          workflowController.currentProjectWidget?.projectController.export();
        },
        child: const Text("保存")),
    InkWell(
        onTap: () async {
          WorkflowController workflowController =
              Get.find<WorkflowController>();
          workflowController.currentProjectWidget?.projectController.import();
        },
        child: const Text("导入")),
    InkWell(
        onTap: () {
          debugPrint(
              "currentProject.projectId = ${controller.currentProjectWidget?.projectController.projectId}");
          debugPrint(
              "currentProject.hashCode = ${controller.currentProjectWidget?.projectController.hashCode}");

          debugPrint(
              "works.length = ${controller.currentProjectWidget?.projectController.works.length}");
        },
        child: const Text("debug")),
    GetBuilder<WorkflowController>(builder: (_) {
      return InkWell(
          child: const Text("对话"),
          onTap: () async {
            await Get.to(ChatWidget(
                projectController:
                    controller.currentProjectWidget!.projectController));
          });
    })
  ];
}
