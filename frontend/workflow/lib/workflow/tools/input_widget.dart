import 'package:flutter/material.dart';

class InputWidget extends StatelessWidget {
  late TextEditingController editController;
  double? width;
  double? height;
  String? initText;
  String? titleText;
  String? hintText;
  Function(String)? onChanged;
  Function(String)? onSubmitted;
  InputWidget(
      {super.key,
      this.width,
      this.height,
      this.initText,
      this.titleText,
      this.hintText,
      this.onChanged,
      this.onSubmitted}) {
    assert((onChanged == null && onSubmitted != null) ||
        (onChanged != null && onSubmitted == null));
    editController = TextEditingController(text: initText);
    if (onSubmitted != null) {
      onChanged = (value) {
        editController.text = value;
      };
    }
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
      height: height,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              if (onSubmitted != null) const Divider(),
              if (titleText != null)
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 4.0),
                  child: Text(titleText!),
                ),
              (onSubmitted != null)
                  ? TextField(
                      minLines: 1,
                      maxLines: 4,
                      onTap: () {
                        debugPrint("?? onTap");
                      },
                      style: const TextStyle(fontSize: 14),
                      controller: editController,
                      decoration: InputDecoration(
                        hintText: hintText,
                        border: const OutlineInputBorder(),
                      ),
                      onSubmitted: onSubmitted)
                  : TextField(
                      minLines: 1,
                      maxLines: 4,
                      style: const TextStyle(fontSize: 14),
                      controller: editController,
                      decoration: InputDecoration(
                        hintText: hintText,
                        border: const OutlineInputBorder(),
                      ),
                      onChanged: onChanged,
                    )
            ],
          ),
          if (onSubmitted != null)
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: ElevatedButton(
                  onPressed: () {
                    onSubmitted!(editController.text);
                  },
                  child: const Padding(
                    padding: EdgeInsets.symmetric(vertical: 4.0),
                    child: Text("确认"),
                  )),
            )
        ],
      ),
    );
  }
}
