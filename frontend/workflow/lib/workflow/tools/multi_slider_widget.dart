import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_sliders/sliders.dart';

class MultiSliderWidget extends StatelessWidget {
  double? width;
  double? height;
  Widget? titleWidget;
  List<String> labels;
  List<TextStyle>? labelsTextStyle;
  List<double> sliderMin;
  List<double> sliderMax;
  List<double> sliderValue;
  List<void Function(dynamic)> sliderOnChanged;
  MultiSliderWidget(
      {super.key,
      this.width,
      this.height,
      required this.titleWidget,
      required this.labels,
      this.labelsTextStyle,
      required this.sliderMin,
      required this.sliderMax,
      required this.sliderValue,
      required this.sliderOnChanged});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: SizedBox(
          width: width,
          height: height,
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            titleWidget ?? const SizedBox(),
            ...List<Widget>.generate(labels.length, (index) {
              return Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(labels[index], style: labelsTextStyle?[index]),
                  SfSlider(
                    //showTicks: true,
                    enableTooltip: true,
                    value: sliderValue[index],
                    min: sliderMin[index],
                    max: sliderMax[index],
                    onChanged: sliderOnChanged[index],
                  ),
                ],
              );
            }).toList()
          ])),
    );
  }
}
