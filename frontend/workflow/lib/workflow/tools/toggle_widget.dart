import 'package:flutter/material.dart';
import 'package:toggle_switch/toggle_switch.dart';

class ToggleWidget extends StatelessWidget {
  double? width;
  double? height;
  int? initialLabelIndex;
  Widget titleWidget;
  List<String> labels;
  List<TextStyle>? labelsTextStyle;
  List<IconData?>? icons;
  void Function(int?)? onToggle;
  ToggleWidget({
    super.key,
    this.width,
    this.height,
    required this.initialLabelIndex,
    required this.titleWidget,
    required this.labels,
    this.labelsTextStyle,
    this.icons,
    required this.onToggle,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: SizedBox(
        width: width,
        height: height,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 4.0),
              child: titleWidget,
            ),
            ToggleSwitch(
                initialLabelIndex: initialLabelIndex ?? 0,
                animate: true,
                labels: labels,
                customTextStyles: labelsTextStyle,
                icons: icons,
                fontSize: 14,
                onToggle: onToggle),
          ],
        ),
      ),
    );
  }
}
