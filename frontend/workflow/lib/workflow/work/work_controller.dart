import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_code_editor/flutter_code_editor.dart';
import 'package:flutter_js/extensions/fetch.dart';
import 'package:get/get.dart';
import 'package:highlight/languages/javascript.dart';
import 'package:json_dynamic_widget/json_dynamic_widget.dart';
import 'package:json_editor/json_editor.dart';
import 'package:star_menu/star_menu.dart';
import 'package:uuid/uuid.dart';
import 'package:workflow/util/data_util/color_lib.dart';
import 'package:workflow/util/js/js_util.dart';
import 'package:workflow/util/network/http_util.dart';
import 'package:workflow/workflow/element/element.dart';
import 'package:workflow/workflow/element/element_controller.dart';
import 'package:workflow/workflow/flow/flow_controller.dart';
import 'package:workflow/workflow/project/project_controller.dart';
import 'package:workflow/workflow/work/work_chat_page.dart';
import 'package:workflow/workflow/work/work_page.dart';

enum CurrentTab { input, output, function }

class WorkController extends GetxController {
  late WorkWidget workWidget;
  late String id;
  late String name;
  String helloMessage = "有什么可以帮你的吗？";
  bool isAiMode = false;
  String aiFunctionConfig = """ [
    {
      "name": "getWether",
      "description": "根据地点和时间获得该时间该地点的天气情况",
      "parameters": {
        "type": "object",
        "properties": {
          "location": {"type": "string", "description": "地点信息,例如北京市、厦门等"},
          "date": {"type": "string", "description": "日期信息,例如上周二,3月5号等"},
          "unit": {
            "type": "string",
            "description": "温度的类型,摄氏度为C,华氏度为F",
            "enum": ["C", 'F']
          }
        },
        "required": ["location", "date"]
      }
    }
  ]
  """;
  String mode = 'design';
  List inputs = [];
  List outputs = [];
  Future<Function>? fn;
  late Offset position;
  //WorkflowController workflowController = Get.find<WorkflowController>();
  late ProjectController projectController;
  FlowController flowController = Get.find<FlowController>();
  late Map<String, dynamic> widgetJsonData;
  List<ElementWidget> inputElements = [];
  List<ElementWidget> outputElements = [];
  List<ElementWidget> previewElements = [];
  CurrentTab currentTab = CurrentTab.input;
  late Color bgColor;
  late bool isTransparent;
  late double width;
  late double height;
  StarMenuController? funcMenuController;
  Map<String, Map<String, dynamic>> map = {
    "text": {"widget": TextWidget, "params": TextWidgetParams},
    "icon": {"widget": IconWidget, "params": IconWidgetParams},
    "newwork_image": {
      "widget": NetworkImageWidget,
      "params": NetworkImageWidgetParams
    },
    "elevate_button": {"widget": ButtonWidget, "params": ButtonWidgetParams},
    "container": {"widget": ContainerWidget, "params": ContainerWidgetParams},
    "row": {"widget": RowWidget, "params": RowWidgetParams},
    "column": {"widget": ColumnWidget, "params": ColumnWidgetParams},
  };
  late JsonWidgetRegistry workJsonWidgetRegistry;
  final jsLib = JsLib();
  final codeController = CodeController(text: """
args = getVar("args")
JSON.stringify({"output":args[0]})           
      """, language: javascript // Initial code
      );
  @override
  onInit() async {
    super.onInit();
    debugPrint("=====>work controller onInit");
    await jsLib.jsRuntime.enableFetch();
  }

  @override
  onReady() {
    debugPrint("=====>work controller onReady");
  }

  setCurrentTab(int i) {
    currentTab = CurrentTab.values[i];
    update();
  }

  setBgColor(Color color) {
    isTransparent = color == Colors.transparent;
    bgColor = color;
    update();
  }

  setWidth(double value) {
    width = value;
    update();
  }

  setHeight(double value) {
    height = value;
    update();
  }

  setHelloMessage(String value) {
    debugPrint("??? =? ${value.isEmpty}");
    helloMessage = value.isEmpty ? "有什么可以帮你的吗？" : value;
  }

  setInit(
      {id,
      targetIds,
      name,
      width,
      height,
      bgColor,
      position,
      required ProjectController projectController,
      required WorkWidget workWidget}) {
    debugPrint("workController setInit");
    final uuid = const Uuid().v4();
    this.id = id ?? "ID-$uuid";
    this.name = name ?? 'WORK-$uuid';
    this.width = width;
    this.height = height;
    this.bgColor = bgColor;
    isTransparent = (bgColor == Colors.transparent);
    this.position = position;
    this.projectController = projectController;
    flowController.projectController = projectController;
    this.workWidget = workWidget;
    workJsonWidgetRegistry =
        JsonWidgetRegistry(overrideInternalFunctions: false, functions: {
      ...JsonWidgetInternalFunctionsBuilder().withSetValue().build(),
      ...<String, JsonWidgetFunction>{
        'dartAdd': ({args, required registry}) {
          return () {
            num result = args![0]['a'] + args[0]['b'];
            debugPrint("dartAdd result:$result");
          };
        },
        'jsAdd': ({args, required registry}) {
          return () {
            jsLib.setVar("args", args);
            String result = jsLib.eval("""
                 x = getVar('args')[0];
                 x['a']+x['b']
             """);
            debugPrint("jsAdd result:$result");
          };
        },
      }
    });
    workJsonWidgetRegistry.registerFunction("httpGet", (
        {args, required registry}) {
      //ex. ${httpGet({'url':'http://xxx','headers':<headers map>,'queryParameters':<queryParameters map>})}
      return () async {
        final httpArgs = args![0];
        String url = httpArgs['url'];
        Map<String, dynamic>? headers = httpArgs['headers'];
        Map<String, dynamic>? queryParameters = httpArgs['queryParameters'];
        final result = await HttpLib.getData(url,
            headers: headers, queryParameters: queryParameters);
        debugPrint("httpGet: $result");
        return result;
      };
    });
    workJsonWidgetRegistry.registerFunction("httpPost", (
        {args, required registry}) {
      //ex. ${httpGet({'url':'http://xxx','data':<body data map>,'headers':<headers map>,'queryParameters':<queryParameters map>})}
      return () async {
        final httpArgs = args![0];
        String url = httpArgs['url'];
        Map<String, dynamic>? data = httpArgs['data'];
        Map<String, dynamic>? headers = httpArgs['headers'];
        Map<String, dynamic>? queryParameters = httpArgs['queryParameters'];
        final result = await HttpLib.postData(url,
            headers: headers, queryParameters: queryParameters, data: data);
        debugPrint(result);
        return result;
      };
    });
    workJsonWidgetRegistry.registerFunction("set", ({args, required registry}) {
      //ex. ${set('name','value')}
      return () {
        String? name = args![0];
        if (name != null) {
          ElementWidget? test =
              getNamedElement(name, [...inputElements, ...outputElements]);
          if (test != null) {
            String? valueKey = test.elementRegistry.getValue('valueKey');
            if (valueKey == null) {
              throw ("valueKey not find!,please define a valid valueKey for this element!");
            }
            test.elementRegistry.setValue(valueKey, args[1]);
            workJsonWidgetRegistry.setValue(name, args[1]);
          }
        }
      };
    });
    workJsonWidgetRegistry.registerFunction("get", ({args, required registry}) {
      //ex. ${get('name')}
      return () {
        String? name = args![0];
        if (name != null) {
          ElementWidget? test =
              getNamedElement(name, [...inputElements, ...outputElements]);
          if (test != null) {
            String? valueKey = test.elementRegistry.getValue('valueKey');
            if (valueKey == null) {
              throw ("valueKey not find!,please define a valid valueKey for this element!");
            }
            debugPrint("${test.elementRegistry.getValue(valueKey)}");
            debugPrint(codeController.code.text);
            return test.elementRegistry.getValue(valueKey);
          }
        }
      };
    });

    workJsonWidgetRegistry.registerFunction("func", (
        {args, required registry}) {
      return () {
        String outString = "";
        try {
          //将args为入参的name列表，所以要将入参的内容拿到后传给jsLib
          List argsValue = [];
          for (var name in args!) {
            ElementWidget? elem = getNamedElement("$name", inputElements);
            if (elem == null) throw ("参数错误,func函数需要依次传入elemnet的名字字符串!");
            argsValue.add(elem.elementRegistry.execute('valueOf', null));
          }
          debugPrint("$argsValue");
          //以数组方式传入
          jsLib.setVar("args", argsValue);
          outString = jsLib.eval(codeController.code.text);
          if (outString.startsWith("ERROR:")) {
            Get.snackbar("func函数错误",
                "$outString \n 返回的数据必须是JSON.stringify(Map<String,dynamic>)的形式!");
            return;
          }
          final result = json.decode(outString) as Map<String, dynamic>;
          for (var item in result.entries) {
            workJsonWidgetRegistry.execute("set", [item.key, item.value])();
          }
        } catch (e) {
          Get.snackbar("错误", "$e");
        }
        Get.back(result: outString);
      };
    });

    update();
  }

  setAiMode(bool value) {
    isAiMode = value;
    update();
  }

  updateAiFunctionConfig(JsonElement config) {
    aiFunctionConfig = config.toPrettyString();
    //update();
  }

  removeWork() {
    projectController.works
        .removeWhere((item) => item.workController.workWidget == workWidget);
    projectController.update();
  }

  ElementWidget? findElement(ElementController elementController) {
    List<ElementWidget> todoElements;
    switch (currentTab) {
      case CurrentTab.input:
        todoElements = inputElements;
        break;
      case CurrentTab.output:
        todoElements = outputElements;
        break;
      default:
        return null;
    }
    ElementWidget? elementWidget;
    if (elementController.parentElementController == null) {
      try {
        elementWidget = todoElements
            .firstWhere((item) => item == elementController.elementWidget);
      } catch (e) {
        elementWidget = null;
      }
    } else {
      try {
        elementWidget = elementController.parentElementController!
            .findElement(elementController);
      } catch (e) {
        elementWidget = null;
      }
    }
    return elementWidget;
  }

  removeElement(ElementController elementController) {
    List<ElementWidget> todoElements;
    switch (currentTab) {
      case CurrentTab.input:
        todoElements = inputElements;
        break;
      case CurrentTab.output:
        todoElements = outputElements;
        break;
      default:
        return;
    }
    if (elementController.parentElementController == null) {
      todoElements
          .removeWhere((item) => item == elementController.elementWidget);
    } else {
      elementController.parentElementController!
          .removeElement(elementController);
    }
    update();
  }

  ElementWidget? addElement(
      {required String type,
      widgetParams,
      ElementController? elementController}) {
    List todoElements;
    ElementWidget? elementWidget;
    switch (currentTab) {
      case CurrentTab.input:
        todoElements = inputElements;
        break;
      case CurrentTab.output:
        todoElements = outputElements;
        break;
      default:
        return null;
    }
    if (type == "buttonTest") {
      if (elementController == null) {
        elementWidget = map[type]!['widget'](
            widgetParams: map[type]!['params'](), workController: this);
        todoElements.add(elementWidget);
      } else {
        elementController.addElement(type: type, workController: this);
      }
    } else {
      switch (type) {
        case 'text':
          if (elementController == null) {
            elementWidget = TextWidget(
                widgetParams: TextWidgetParams(), workController: this);
            todoElements.add(elementWidget);
          } else {
            elementController.addElement(
                type: type, widgetParams: widgetParams, workController: this);
          }
          break;
        case 'icon':
          if (elementController == null) {
            elementWidget = IconWidget(
                widgetParams: IconWidgetParams(), workController: this);
            todoElements.add(elementWidget);
          } else {
            elementController.addElement(
                type: type, widgetParams: widgetParams, workController: this);
          }
          break;
        case 'network_image':
          if (elementController == null) {
            elementWidget = NetworkImageWidget(
                widgetParams: NetworkImageWidgetParams(), workController: this);
            todoElements.add(elementWidget);
          } else {
            elementController.addElement(
                type: type, widgetParams: widgetParams, workController: this);
          }
          break;
        case 'elevated_button':
          if (elementController == null) {
            elementWidget = ButtonWidget(
                widgetParams: ButtonWidgetParams(), workController: this);
            todoElements.add(elementWidget);
          } else {
            elementController.addElement(
                type: type, widgetParams: widgetParams, workController: this);
          }
          break;
        case 'custom':
          if (elementController == null) {
            elementWidget = ElementWidget(
                widgetParams: ElementWidgetParams(), workController: this);
            todoElements.add(elementWidget);
          } else {
            elementController.addElement(
                type: type, widgetParams: widgetParams, workController: this);
          }
          break;
        case 'container':
          if (elementController == null) {
            elementWidget = ContainerWidget(
                widgetParams: ContainerWidgetParams(
                    widgetArgs: widgetParams?['widgetArgs']),
                workController: this);
            todoElements.add(elementWidget);
          } else {
            elementController.addElement(
                type: type, widgetParams: widgetParams, workController: this);
          }
          break;
        case 'listview':
          if (elementController == null) {
            elementWidget = ListViewWidget(
                widgetParams: ListViewWidgetParams(
                    widgetArgs: widgetParams?['widgetArgs']),
                workController: this);
            todoElements.add(elementWidget);
          } else {
            elementController.addElement(
                type: type, widgetParams: widgetParams, workController: this);
          }
          break;
        case 'expanded':
          if (elementController == null) {
            elementWidget = ExpandedWidget(
                widgetParams: ExpandedWidgetParams(), workController: this);
            todoElements.add(elementWidget);
          } else {
            elementController.addElement(
                type: type, widgetParams: widgetParams, workController: this);
          }
          break;
        case 'row':
          if (elementController == null) {
            elementWidget = RowWidget(
                widgetParams:
                    RowWidgetParams(widgetArgs: widgetParams?['widgetArgs']),
                workController: this);
            todoElements.add(elementWidget);
          } else {
            elementController.addElement(
                type: type, widgetParams: widgetParams, workController: this);
          }
          break;
        case 'column':
          if (elementController == null) {
            elementWidget = ColumnWidget(
                widgetParams:
                    ColumnWidgetParams(widgetArgs: widgetParams?['widgetArgs']),
                workController: this);
            todoElements.add(elementWidget);
          } else {
            elementController.addElement(
                type: type, widgetParams: widgetParams, workController: this);
          }
          break;
        default:
          throw ("增加的组件尚未实现!");
      }
    }
    update();
    return elementWidget;
  }

  import(Map<String, dynamic> importElements) {
    List<Map<String, dynamic>> importInputsElements =
        List<Map<String, dynamic>>.from(importElements["inputs"]);
    List<Map<String, dynamic>> importOutputsElements =
        List<Map<String, dynamic>>.from(importElements["outputs"]);
    name = importElements['name'];
    helloMessage = importElements['helloMessage'];
    //导入AI
    isAiMode = importElements['isAiMode'] ?? false;
    aiFunctionConfig = importElements['aiFunctionConfig'] ?? aiFunctionConfig;
    //导入elements
    currentTab = CurrentTab.input;
    for (var item in importInputsElements) {
      _importWidgets(item, null);
    }
    currentTab = CurrentTab.output;
    for (var item in importOutputsElements) {
      _importWidgets(item, null);
    }
    //导入自定义函数
    codeController.fullText = importElements["function"];
    //导入style
    Map<String, dynamic> styleMap = importElements["style"];
    width = styleMap['width'];
    height = styleMap['height'];
    bgColor = ColorLib.parse(styleMap['bgColor']) ?? Colors.transparent;
    position = Offset(styleMap['x'], styleMap['y']);
    update();
  }

  _importWidgets(item, ElementController? elementController,
      [isListview = false]) {
    if (item == null) return;
    switch (item['type']) {
      case 'ROW':
        ElementWidget? elementWidget;
        if (elementController == null) {
          elementWidget = addElement(
              type: 'row', widgetParams: {'widgetArgs': item['widgetArgs']});
        } else {
          elementWidget = elementController.addElement(
              type: 'row',
              widgetParams: {'widgetArgs': item['widgetArgs']},
              workController: this);
        }
        if (elementWidget == null) return;
        for (var item in item['children']) {
          _importWidgets(item, elementWidget.elementController, true);
        }
        break;
      case 'COLUMN':
        ElementWidget? elementWidget;
        if (elementController == null) {
          elementWidget = addElement(
              type: 'column', widgetParams: {'widgetArgs': item['widgetArgs']});
        } else {
          elementWidget = elementController.addElement(
              type: 'column',
              widgetParams: {'widgetArgs': item['widgetArgs']},
              workController: this);
        }
        if (elementWidget == null) return;
        for (var item in item['children']) {
          _importWidgets(item, elementWidget.elementController);
        }
        break;
      case 'CONTAINER':
        ElementWidget? elementWidget;
        if (elementController == null) {
          elementWidget = addElement(
              type: 'container',
              widgetParams: {'widgetArgs': item['widgetArgs']});
        } else {
          elementWidget = elementController.addElement(
              type: 'container',
              widgetParams: {'widgetArgs': item['widgetArgs']},
              workController: this);
        }

        if (elementWidget == null) return;
        _importWidgets(item['child'], elementWidget.elementController);
        break;
      case 'LISTVIEW':
        ElementWidget? elementWidget;
        if (elementController == null) {
          elementWidget = addElement(
              type: 'listview',
              widgetParams: {'widgetArgs': item['widgetArgs']});
        } else {
          elementWidget = elementController.addElement(
              type: 'listview',
              widgetParams: {'widgetArgs': item['widgetArgs']},
              workController: this);
        }
        //listview特殊，需要通过registry导入
        if (elementWidget == null) return;
        final temp = Map.from(item['widgetArgs']);
        temp['template'] = _importWidgets(
            item['child'], elementWidget.elementController, true);

        elementWidget.elementController.setupRegistry(item['widgetArgs']);

        break;
      default:
        if (isListview) {
          return item['jsonWidgetData'];
        }
        ElementWidget? elementWidget;
        if (elementController == null) {
          elementWidget = addElement(type: item['type']);
        } else {
          elementWidget = elementController.addElement(
              type: item['type'], workController: this);
        }
        elementWidget?.elementController
            .setupRegistry(item['jsonWidgetRegistry']);
    }
  }

  Map<String, dynamic> export() {
    // {
    // style:{
    //   x:
    //   y:
    //   width:
    //   height:
    //   bgColor:
    // },
    // inputs:[{
    //    "type":"ROW",
    //    "args":{},
    //    "children":[
    //       "type":"CONTAINER":
    //       "args":{},
    //       "child":{
    //          "type":"type",
    //          "widgetJsonData":{....},
    //          "widgetRegistry":{....}
    //       }
    //    ]
    // },...
    // }]
    //  outputs:...
    //  function:...
    // }
    List exportInputsElements =
        inputElements.map((item) => _exportWidgets(item)).toList();
    List exportOutputsElements =
        outputElements.map((item) => _exportWidgets(item)).toList();
    String? exportFunction = codeController.fullText;
    Map<String, dynamic> exportElements = {
      "name": name,
      "helloMessage": helloMessage,
      "style": {
        "x": position.dx,
        "y": position.dy,
        "width": width,
        "height": height,
        "bgColor": ColorLib.colorString(bgColor)
      },
      "isAiMode": isAiMode,
      "aiFunctionConfig": aiFunctionConfig,
      "inputs": exportInputsElements,
      "outputs": exportOutputsElements,
      "function": exportFunction,
    };
    debugPrint("exportElements==>$exportElements");
    return exportElements;
  }

  Map<String, dynamic> _exportWidgets(ElementWidget item) {
    switch (item.elementController.type) {
      case "row":
        return {
          "type": "ROW",
          "jsonWidgetData": null,
          "widgetArgs": item.elementController.widgetParams.widgetArgs,
          "children": item.elementController.children
              .map((item) => _exportWidgets(item))
              .toList()
        };
      case "column":
        return {
          "type": "COLUMN",
          "jsonWidgetData": null,
          "widgetArgs": item.elementController.widgetParams.widgetArgs,
          "children": item.elementController.children
              .map((item) => _exportWidgets(item))
              .toList()
        };
      case "container":
        if (item.elementController.child is ElementWidget) {
          return {
            "type": "CONTAINER",
            "jsonWidgetData": null,
            "widgetArgs": item.elementController.widgetParams.widgetArgs,
            "child":
                _exportWidgets(item.elementController.child as ElementWidget)
          };
        } else {
          return {"type": "CONTAINER", "child": null};
        }
      case "listview":
        if (item.elementController.child is ElementWidget) {
          return {
            "type": "LISTVIEW",
            "jsonWidgetData": null,
            "widgetArgs": item.elementController.elementRegistry.values,
            "child":
                _exportWidgets(item.elementController.child as ElementWidget)
          };
        } else {
          return {"type": "LISTVIEW", "child": null};
        }
      default:
        return {
          "type": item.elementController.widgetParams.widgetJsonData["type"],
          "jsonWidgetData": item.elementController.widgetParams.widgetJsonData,
          "jsonWidgetRegistry": item.elementController.elementRegistry.values
        };
    }
  }

  preview(BuildContext context) async {
    String previewTag;
    switch (currentTab) {
      case CurrentTab.input:
        previewTag = "inputPage";
        break;
      case CurrentTab.output:
        previewTag = "outputPage";
        break;
      default:
        return;
    }
    Map<String, Widget> workPage = generateWorkPage(context);
    await Get.defaultDialog(
      //barrierColor: Colors.transparent,
      title: "预览",
      content: Material(
        child: Center(
          child: SizedBox(
              width: width,
              height: height,
              child: SingleChildScrollView(
                child: workPage[previewTag],
              )),
        ),
      ),
    );
  }

  _recursionElements(ElementWidget item, Function(ElementWidget) fn) {
    switch (item.elementController.type) {
      case "row":
      case "column":
        for (ElementWidget child in item.elementController.children) {
          _recursionElements(child, fn);
        }
        break;
      case 'container':
        var child = item.elementController.child;
        if (child is! ElementWidget) return;
        _recursionElements(child, fn);
        break;
      default:
        fn(item);
    }
  }

  List<ElementWidget> getNamedElements(List<ElementWidget> todo) {
    List<ElementWidget> result = [];
    for (var item in todo) {
      _recursionElements(item, (item) {
        if (item.elementController.elementRegistry.values['name'] != null) {
          result.add(item);
        }
      });
    }
    return result;
  }

  ElementWidget? getNamedElement(String name, List<ElementWidget> todo) {
    ElementWidget? result;
    for (var item in todo) {
      _recursionElements(item, (item) {
        if (item.elementController.elementRegistry.values['name'] == name) {
          result = item;
        }
      });
    }
    return result;
  }

  Map<String, Widget> generateWorkPage(context) {
    Widget inputPage = const Center(child: Text("错误的input页面"));
    Widget outputPage = const Center(child: Text("错误的output页面"));
    Map<String, dynamic> map = export();
    WorkWidget work = WorkChatWidget(
        projectController: projectController,
        width: map['style']['width'],
        height: map['style']['height'],
        bgColor: ColorLib.parse(map['style']['bgColor']) ?? Colors.transparent);
    //setup inputElements
    work.workController.mode = 'chat';
    work.workController.import(map);
    inputPage = workInputsWidget(work.workController);
    outputPage = workOutputsWidget(work.workController);

    return {"inputPage": inputPage, "outputPage": outputPage};
  }
}
