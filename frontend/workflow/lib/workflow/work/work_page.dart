import 'package:flutter/material.dart';
import 'package:flutter_code_editor/flutter_code_editor.dart';
import 'package:get/get.dart';
import 'package:json_editor/json_editor.dart';
import 'package:toggle_switch/toggle_switch.dart';
import 'package:workflow/workflow/menu/element_menu.dart';
import 'package:workflow/workflow/menu/work_menu.dart';
import 'package:workflow/workflow/project/project_controller.dart';

import 'work.dart';

class WorkWidget extends StatelessWidget {
  late WorkController workController;
  ProjectController projectController;
  double width;
  double height;
  Color bgColor;
  Offset position;
  List<String>? targetIds;
  WorkWidget(
      {super.key,
      id,
      targetIds,
      name,
      required this.projectController,
      this.width = 400,
      this.height = 300,
      this.bgColor = Colors.transparent,
      this.position = const Offset(10, 10)}) {
    debugPrint("====> work base page construct");
    workController = Get.find<WorkController>();
    workController.setInit(
        id: id,
        targetIds: targetIds,
        name: name,
        projectController: projectController,
        workWidget: this,
        width: width,
        height: height,
        bgColor: bgColor,
        position: position);
  }
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}

class WorkTabWidget extends StatelessWidget {
  WorkController workController;
  late Offset doubletapDetails;
  late Offset tapDetails;
  WorkTabWidget(
      {super.key, required this.workController, width, height, bgColor}) {
    workController.width = width;
    workController.height = height;
    workController.bgColor = bgColor ?? Colors.transparent;
  }
  @override
  Widget build(BuildContext context) {
    return GetBuilder(
        init: workController,
        global: false,
        builder: (_) => Material(
              child: SizedBox(
                width: workController.width,
                height: workController.height,
                child: Column(children: [
                  const SizedBox(height: 6),
                  ToggleSwitch(
                    minWidth: workController.width / 3 - 5,
                    minHeight: 25,
                    initialLabelIndex: workController.currentTab.index,
                    totalSwitches: 3,
                    labels: const ['输入', '输出', '函数'],
                    fontSize: 14,
                    onToggle: (index) {
                      workController.setCurrentTab(index!);
                      debugPrint(
                          'switched to: ,${workController.currentTab},${workController.currentTab.index}');
                    },
                  ),
                  const SizedBox(height: 4),
                  Expanded(
                    child: Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.black26)),
                      child: IndexedStack(
                          index: workController.currentTab.index,
                          children: [
                            workController.isAiMode
                                ? Stack(
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: JsonEditor.string(
                                          jsonString:
                                              workController.aiFunctionConfig,
                                          onValueChanged: (value) {
                                            workController
                                                .updateAiFunctionConfig(value);
                                          },
                                        ),
                                      ),
                                      Align(
                                        alignment: Alignment.bottomRight,
                                        child: Padding(
                                          padding: const EdgeInsets.all(10.0),
                                          child: FloatingActionButton.small(
                                              child: const Icon(Icons.widgets,
                                                  size: 20),
                                              onPressed: () {
                                                workController.setAiMode(false);
                                              }),
                                        ),
                                      )
                                    ],
                                  )
                                : Stack(
                                    children: [
                                      Container(
                                        width: double.maxFinite,
                                        height: double.maxFinite,
                                        color: workController.bgColor,
                                        child: GestureDetector(
                                          onTapDown: (details) {
                                            tapDetails = details.localPosition;
                                          },
                                          onTap: () {
                                            Offset position = tapDetails;
                                            displayElementMenu(context,
                                                position, workController, null);
                                          },
                                          onDoubleTapDown: (details) {
                                            doubletapDetails =
                                                details.localPosition;
                                          },
                                          onDoubleTap: () {
                                            Offset position = doubletapDetails;
                                            displayWorkMenu(context, position,
                                                workController, null);
                                          },
                                          child: SingleChildScrollView(
                                              child: Container(
                                            color: Colors.black12,
                                            child: Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceEvenly,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: workController
                                                    .inputElements),
                                          )),
                                        ),
                                      ),
                                      Align(
                                        alignment: Alignment.bottomRight,
                                        child: Padding(
                                          padding: const EdgeInsets.all(10.0),
                                          child: FloatingActionButton.small(
                                              child: const Icon(Icons.school,
                                                  size: 20),
                                              onPressed: () {
                                                workController.setAiMode(true);
                                              }),
                                        ),
                                      )
                                    ],
                                  ),
                            Container(
                              width: double.maxFinite,
                              height: double.maxFinite,
                              color: workController.bgColor,
                              child: GestureDetector(
                                onTapUp: (details) {
                                  Offset position = details.localPosition;
                                  displayElementMenu(
                                      context, position, workController, null);
                                },
                                onDoubleTapDown: (details) {
                                  doubletapDetails = details.localPosition;
                                },
                                onDoubleTap: () {
                                  Offset position = doubletapDetails;
                                  displayWorkMenu(
                                      context, position, workController, null);
                                },
                                child: SingleChildScrollView(
                                    child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceEvenly,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children:
                                            workController.outputElements)),
                              ),
                            ),
                            CodeTheme(
                              data: CodeThemeData(),
                              child: SingleChildScrollView(
                                child: CodeField(
                                  expands: false,
                                  controller: workController.codeController,
                                ),
                              ),
                            )
                          ]),
                    ),
                  )
                ]),
              ),
            ));
  }
}

Widget workInputsWidget(WorkController workController) {
  return Container(
    width: workController.width,
    height: workController.height,
    color: workController.bgColor,
    child: SingleChildScrollView(
      child: GetBuilder(
          init: workController,
          global: false,
          builder: (_) {
            return Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: workController.inputElements);
          }),
    ),
  );
}

Widget workOutputsWidget(WorkController workController) {
  return Container(
    //width: workController.width,
    //height: workController.height,
    //color: workController.bgColor,
    child: SingleChildScrollView(
      child: GetBuilder(
          init: workController,
          global: false,
          builder: (_) {
            return Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: workController.outputElements);
          }),
    ),
  );
}
