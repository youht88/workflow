import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:workflow/workflow/flow/flow_page.dart';
import 'package:workflow/workflow/work/work_page.dart';

class WorkDesignWidget extends WorkWidget {
  WorkDesignWidget(
      {super.key,
      super.id,
      super.targetIds,
      super.name,
      required super.projectController});

  @override
  Widget build(BuildContext context) {
    return GetBuilder(
        init: workController,
        global: false,
        builder: (context) {
          return FlowWidget(
            id: workController.id,
            targetIds: super.targetIds,
            workController: super.workController,
            width: workController.width,
            height: workController.height,
            child: WorkTabWidget(
                workController: workController,
                width: workController.width,
                height: workController.height,
                bgColor: workController.bgColor),
          );
        });
  }
}
