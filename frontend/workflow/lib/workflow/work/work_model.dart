class Work {
  late String name;
  String? title;
  late dynamic inputs;
  late dynamic outputs;
  late dynamic function;
  late String describe;
}

//交互组件
class Interactive extends Work {
  String? label;
  String? info;
  dynamic value;
  dynamic defaultValue;
  bool visible = true;
  bool required = true;
}

class TextBox extends Interactive {
  int lines = 1;
  int maxLines = 20;
  String? placeholder;
  String type = 'text';
}

class Number extends Interactive {
  int precision = 0;
  double? min;
  double? max;
}

class Image extends Interactive {
  int? width;
  int? height;
  String source = 'upload';
}

class Button extends Interactive {}

//////布局组件
class Layout extends Work {}

class Column extends Layout {}

class Row extends Layout {}

class Tab extends Layout {}

class Accordion extends Layout {}
