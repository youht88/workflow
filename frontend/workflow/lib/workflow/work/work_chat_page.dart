import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:json_dynamic_widget/json_dynamic_widget.dart';
import 'package:workflow/workflow/work/work_page.dart';

class WorkChatWidget extends WorkWidget {
  late JsonWidgetRegistry registry;
  WorkChatWidget(
      {super.key,
      super.id,
      super.targetIds,
      super.width,
      super.height,
      super.bgColor,
      required super.projectController}) {
    workController.widgetJsonData = {
      "type": "center",
      "child": {
        "type": "container",
        "args": {
          "color": r"${color}",
          "width": r"${width}",
          "height": r"${height}"
        },
        "child": r"${child}"
      },
    };
    //registry = JsonWidgetRegistry();
    registry =
        JsonWidgetRegistry(parent: workController.workJsonWidgetRegistry);
    registry.registerFunction("abc", ({args, required registry}) {
      debugPrint("$args");
      return 'hello ${args![0]}';
    });
    registry.setValue("child", {
      "type": "text",
      "args": {"text": r"${abc({'a':456,'b':123})}"}
    });
    registry.setValue("color", "#666666");
  }
  @override
  Widget build(BuildContext context) {
    return GetBuilder(
      init: workController,
      global: false,
      builder: (_) {
        return Material(
          child: Container(
            width: width,
            height: height,
            color: bgColor,
            child: JsonWidgetData.fromDynamic(workController.widgetJsonData,
                    registry: registry)!
                .build(context: context),
          ),
        );
      },
    );
  }
}
