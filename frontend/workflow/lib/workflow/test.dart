import 'package:flutter/material.dart';

import 'grid_background.dart';

class MyWidget extends StatelessWidget {
  const MyWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return const Stack(
      children: [
        Positioned.fill(child: GridBackground()),
        Center(child: Text("abcd")),
      ],
    );
  }
}
