import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:workflow/workflow/project/project_controller.dart';
import 'package:workflow/workflow/work/work_controller.dart';

class FlowController extends GetxController {
  List<String>? targetIds;
  AlignmentGeometry sourceAnchor = Alignment.centerRight;
  AlignmentGeometry targetAnchor = Alignment.centerLeft;
  late String id;
  double? height;
  double? width;
  bool doubleSided = false;
  Widget? child;
  late double bow;
  late Color color;
  late Widget childWithHandle;
  late double handleSize;
  late WorkController workController;
  late ProjectController projectController;
  Offset handleOffset = Offset.zero;
  setInit(
      {id,
      targetIds,
      width,
      height,
      doubleSided,
      sourceAnchor,
      targetAnchor,
      handleSize,
      color,
      bow,
      child,
      workController}) {
    this.id = id;
    this.targetIds = targetIds;
    this.width = width ?? 100;
    this.height = height ?? 100;
    this.doubleSided = doubleSided ?? false;
    this.sourceAnchor = Alignment.centerRight;
    this.targetAnchor = Alignment.centerLeft;
    this.targetIds = targetIds ?? [];
    this.handleSize = handleSize ?? 10.0;
    this.child = child;
    this.color = color;
    this.bow = bow;
    this.workController = workController;
    update();
    childWithHandle = GetBuilder<FlowController>(
        init: this,
        global: false,
        builder: (_) => SizedBox(
            width: this.width! + this.handleSize,
            height: this.height! + this.handleSize,
            child: Stack(
              alignment: Alignment.center,
              children: [
                this.child!,
                CircleHandle(
                    type: "leftCenter",
                    size: this.handleSize,
                    flowController: workController.flowController),
                CircleHandle(
                    type: "topCenter",
                    size: this.handleSize,
                    flowController: workController.flowController),
                CircleHandle(
                    type: "rightCenter",
                    size: this.handleSize,
                    flowController: workController.flowController),
                CircleHandle(
                    type: "bottomCenter",
                    size: this.handleSize,
                    flowController: workController.flowController),
              ],
            )));
  }

  setWidth(value) {
    width = value;
    update();
  }

  setHeight(value) {
    height = value;
    update();
  }
}

class CircleHandle extends StatelessWidget {
  FlowController flowController;
  CircleHandle({
    super.key,
    required this.type,
    required this.size,
    required this.flowController,
  });
  String type;
  double size;
  late Alignment alignment;
  @override
  Widget build(BuildContext context) {
    switch (type) {
      case 'topCenter':
        alignment = const Alignment(0, -1);
        break;
      case 'leftCenter':
        alignment = const Alignment(-1, 0);
        break;
      case 'rightCenter':
        alignment = const Alignment(1, 0);
        break;
      case 'bottomCenter':
      default:
        alignment = const Alignment(0, 1);
        break;
    }
    return Align(
      alignment: alignment,
      child: Draggable(
        data: "ok",
        feedback: Container(height: 10, width: 10, color: Colors.red),
        onDragUpdate: (detail) {
          debugPrint("$detail");
        },
        onDragStarted: () {
          flowController.projectController.currentFlowController =
              flowController;
          switch (type) {
            case "rightCenter":
              flowController.sourceAnchor = Alignment.centerRight;
              break;
            case "leftCenter":
              flowController.sourceAnchor = Alignment.centerLeft;
              break;
            case "topCenter":
              flowController.sourceAnchor = Alignment.topCenter;
              break;
            case "bottomCenter":
              flowController.sourceAnchor = Alignment.bottomCenter;
              break;
          }
          flowController.update();
        },
        child: DragTarget(
          onAccept: (_) {
            ProjectController projectController =
                flowController.projectController;
            if (projectController.currentFlowController == null) return;
            switch (type) {
              case "rightCenter":
                projectController.currentFlowController!.targetAnchor =
                    Alignment.centerRight;
                break;
              case "leftCenter":
                projectController.currentFlowController!.targetAnchor =
                    Alignment.centerLeft;
                break;
              case "topCenter":
                projectController.currentFlowController!.targetAnchor =
                    Alignment.topCenter;
                break;
              case "bottomCenter":
                projectController.currentFlowController!.targetAnchor =
                    Alignment.bottomCenter;
                break;
            }
            projectController.currentFlowController!.targetIds!
                .add(flowController.id);
            projectController.currentFlowController!.update();
          },
          onWillAccept: (_) {
            debugPrint("!!!!!!okok");
            return true;
          },
          builder: (context, _, __) => Padding(
            padding: const EdgeInsets.all(0.0),
            child: Container(
              width: size,
              height: size,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                border: Border.all(
                  color: Colors.black,
                  width: 1,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
