import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:widget_arrows/widget_arrows.dart';
import 'package:workflow/workflow/work/work_controller.dart';

import 'flow_controller.dart';

class FlowWidget extends StatelessWidget {
  late WorkController workController;
  late FlowController flowController;
  FlowWidget({
    super.key,
    id,
    targetIds,
    doubleSided,
    color = Colors.blue,
    bow = 0.2,
    sourceAnchor,
    targetAnchor,
    required child,
    width,
    height,
    handleSize,
    required this.workController,
  }) {
    debugPrint("new a FlowController");
    flowController = workController.flowController;
    flowController.setInit(
        id: id,
        targetIds: targetIds,
        width: width,
        height: height,
        doubleSided: doubleSided,
        handleSize: handleSize,
        color: color,
        bow: bow,
        child: child,
        workController: workController);
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<FlowController>(
      init: workController.flowController,
      global: false,
      builder: (c) {
        return Positioned(
            top: workController.position.dy,
            left: workController.position.dx,
            child: ArrowElement(
              id: flowController.id,
              targetIds: flowController.targetIds,
              straights: true,
              width: 1,
              tipLength: 5,
              bow: flowController.bow,
              stretch: 0.9,
              doubleSided: flowController.doubleSided,
              sourceAnchor: flowController.sourceAnchor,
              targetAnchor: flowController.targetAnchor,
              color: flowController.color,
              child: Draggable(
                // feedback: SizedBox(
                //     height: controller.height,
                //     width: controller.width,
                //     child: child),
                feedback: flowController.childWithHandle,
                childWhenDragging: Container(),
                onDragUpdate: (detail) {
                  workController.position =
                      workController.position + detail.delta;
                  //flowController.update();
                },
                onDragEnd: (detail) {
                  flowController.update();
                },
                child: flowController.childWithHandle,
              ),
            ));
      },
    );
  }
}
