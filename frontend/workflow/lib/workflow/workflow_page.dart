library workflow;

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:star_menu/star_menu.dart';
import 'package:tabbed_view/tabbed_view.dart';
import 'package:widget_arrows/widget_arrows.dart';
import 'package:workflow/workflow/chat/chat_page.dart';
import 'package:workflow/workflow/grid_background.dart';
import 'package:workflow/workflow/menu/menu_params.dart';
import 'package:workflow/workflow/work/work_page.dart';
import 'package:workflow/workflow/workflow_controller.dart';

part 'menu/workflow_menu.dart';

class WorkflowWidget extends StatelessWidget {
  const WorkflowWidget({super.key});

  @override
  Widget build(BuildContext context) {
    WorkflowController controller = Get.put(WorkflowController());
    StarMenuController firstMenuController = StarMenuController();
    return GetBuilder(
        init: controller,
        builder: (controller) => ArrowContainer(
              child: Scaffold(
                body: SafeArea(
                  child: Stack(
                    children: [
                      const Positioned.fill(
                        child: GridBackground(params: GridBackgroundParams()),
                      ),
                      controller.projectTabs.isNotEmpty
                          ? TabbedViewTheme(
                              data: TabbedViewThemeData.classic(),
                              child: TabbedView(
                                  controller: controller.projectTabController,
                                  onTabClose: (index, _) {
                                    controller.removeProjectWidget(index);
                                  },
                                  onTabSelection: (index) {
                                    if (index == null) return;
                                    controller.selectProjectWidget(index);
                                  },
                                  tabCloseInterceptor: (index) {
                                    if (controller.projectTabs.length == 1) {
                                      return false;
                                    }
                                    return true;
                                  }),
                            )
                          : Container()
                    ],
                  ),
                ),
                floatingActionButton:
                    IconButton(icon: const Icon(Icons.add), onPressed: () => {})
                        .addStarMenu(
                            items: _menuItems(
                                controller, context, firstMenuController),
                            params: getMenuParams(context,
                                centerOffset: const Offset(-80, -300)),
                            controller: firstMenuController,
                            onItemTapped: (index, menuController) {
                              debugPrint("index:$index");
                              if (index != 3) {
                                menuController.closeMenu!();
                              }
                            }),
              ),
            ));
  }
}
