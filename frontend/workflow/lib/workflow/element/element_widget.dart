import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:json_dynamic_widget/json_dynamic_widget.dart';
import 'package:workflow/workflow/menu/element_menu.dart';
import 'package:workflow/workflow/work/work_controller.dart';

import 'element_controller.dart';

class ElementWidgetParams {
  late Map<String, dynamic> widgetJsonData;

  ElementWidgetParams({widgetJsonData}) {
    Map<String, dynamic> jsonData = {
      "type": "container",
      "args": {"width": 100, "height": 100},
      "child": {
        "type": "list_view",
        // "args": {
        //   "scrollDirection": ThemeEncoder.encodeAxis(Axis.horizontal)
        // },
        "children": r"${for_each(data,'template')}"
      }
    };
    //debugPrint("${ThemeDecoder.decodeBoxDecoration(jsonData)}");
    this.widgetJsonData = widgetJsonData ?? jsonData;
  }
}

// ignore: must_be_immutable
class ElementWidget extends StatelessWidget {
  late ElementController elementController;
  late JsonWidgetRegistry elementRegistry;
  late Offset doubletapDetails;
  ElementWidget({
    super.key,
    widgetParams,
    required WorkController workController,
    ElementController? parentElementController,
  }) {
    elementController = Get.find<ElementController>();
    elementController.elementWidget = this;
    elementController.widgetParams = widgetParams ?? ElementWidgetParams();
    elementController.workController = workController;
    elementController.parentElementController = parentElementController;
    elementRegistry = JsonWidgetRegistry(
        disableValidation: true,
        overrideInternalFunctions: false,
        parent: workController.workJsonWidgetRegistry);
    elementRegistry.registerFunction("valueOf", ({args, required registry}) {
      var result =
          elementRegistry.getValue(elementRegistry.getValue('valueKey'));
      return result;
    });
    elementController.elementRegistry = elementRegistry;
  }

  @override
  Widget build(BuildContext context) {
    Widget main = JsonWidgetData.fromDynamic(
            elementController.widgetParams.widgetJsonData,
            registry: elementRegistry)!
        .build(context: context);

    return elementController.workController.mode == 'design'
        ? ElementActionWidget(
            elementController: elementController,
            child: GestureDetector(
                onDoubleTapDown: (details) {
                  doubletapDetails = details.localPosition;
                },
                onDoubleTap: () {
                  Offset position = doubletapDetails;
                  _displayMenu(
                      context, position, elementController, elementRegistry);
                },
                //child: elementController.child!,
                child: Container(
                    decoration:
                        BoxDecoration(border: Border.all(color: Colors.red)),
                    child: main)),
          )
        : main;
  }
}

// ignore: must_be_immutable
class ElementActionWidget extends StatelessWidget {
  Widget child;
  ElementController elementController;
  Offset? tapDetails;
  late Offset doubletapDetails;
  ElementActionWidget(
      {super.key, required this.elementController, required this.child});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ElementController>(
        init: elementController,
        global: false,
        builder: (_) => GestureDetector(
            onTapDown: (details) {
              tapDetails = details.globalPosition;
              debugPrint("tapDetails:$tapDetails");
            },
            onTap: () {
              Offset position = tapDetails!;
              displayElementMenu(context, position,
                  elementController.workController, elementController);
            },
            // onDoubleTapDown: (details) {
            //   doubletapDetails = details.globalPosition;
            // },
            // onDoubleTap: () {
            //   Offset position = doubletapDetails;
            //   displayElementMenu(context, position,
            //       elementController.workController, elementController);
            // },
            child: Draggable(
              dragAnchorStrategy: (source, context, pointer) {
                return pointerDragAnchorStrategy(
                    source, context, pointer - const Offset(100, 100));
              },
              data: elementController,
              feedback: Material(
                  child: Container(height: 10, width: 10, color: Colors.blue)),
              childWhenDragging: const SizedBox(),
              child: DragTarget(
                  onWillAccept: (data) {
                    if (elementController.layoutType == null) return false;
                    return true;
                  },
                  onAccept: (ElementController sourceController) {
                    debugPrint("!!!!,$sourceController");
                    ElementWidget? sourceWidget = sourceController
                        .workController
                        .findElement(sourceController);
                    if (sourceWidget != null) {
                      if (sourceController.parentElementController == null) {
                        sourceController.workController
                            .removeElement(sourceController);
                      } else {
                        sourceController.parentElementController!
                            .removeElement(sourceController);
                      }
                      if (elementController.layoutType != null) {
                        //目标是布局容器
                        elementController.pushElement(
                            sourceWidget, elementController.workController);
                        // data.elementController.parentElementController =
                        //     elementController;
                      } else {
                        //目标非容器布局
                        debugPrint("不会出现，在onAccept事件即返回!");
                        // elementController.addElement(
                        //     sourceWidget, elementController.workController);
                      }
                      elementController.update();
                    }
                  },
                  builder: (context, _, __) => child),
            )));
  }
}

_displayMenu(BuildContext context, Offset position,
    ElementController elementController, JsonWidgetRegistry registry) {
  Get.defaultDialog(
    title: '设置custom',
    titleStyle: const TextStyle(fontSize: 18),
    content: SizedBox(
      width: 240,
      height: 350,
      child: ListView(
        children: const [],
      ),
    ),
  );
}
