import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:json_dynamic_widget/json_dynamic_widget.dart';
import 'package:json_theme/json_theme.dart';
import 'package:star_menu/star_menu.dart';
import 'package:workflow/workflow/element/element_controller.dart';
import 'package:workflow/workflow/menu/menu_params.dart';
import 'package:workflow/workflow/tools/toggle_widget.dart';

import 'element_widget.dart';

class ColumnWidgetParams {
  Map<String, dynamic>? widgetJsonData;
  late Map<String, dynamic> widgetArgs;
  MainAxisAlignment? mainAxisAlignment;
  CrossAxisAlignment? crossAxisAlignment;
  Map<String, String> mainAxisAlignmentLabelsMap = {
    "居上": "start",
    "居中": "center",
    "居下": "end",
    "两端": "spaceBetween",
    "均分": "spaceAround",
  };
  Map<String, String> crossAxisAlignmentLabelsMap = {
    "居中": "center",
    "居左": "start",
    "居右": "end",
  };
  ColumnWidgetParams({widgetArgs}) {
    this.widgetArgs = widgetArgs ??
        {"mainAxisAlignment": "start", "crossAxisAlignment": "center"};
  }
  int getMainAxisAlignmentIndex() {
    return mainAxisAlignmentLabelsMap.values
        .toList()
        .indexOf(widgetArgs["mainAxisAlignment"] ?? "start");
  }

  setMainAxisAlignment(int? index, controller) {
    widgetArgs["mainAxisAlignment"] =
        mainAxisAlignmentLabelsMap.values.toList()[index ?? 0];
    debugPrint("$mainAxisAlignment");
    controller.update();
  }

  int getCrossAxisAlignmentIndex() {
    return crossAxisAlignmentLabelsMap.values
        .toList()
        .indexOf(widgetArgs["crossAxisAlignment"] ?? "center");
  }

  setCrossAxisAlignment(int index, controller) {
    List test = crossAxisAlignmentLabelsMap.values.toList();
    widgetArgs["crossAxisAlignment"] = test[index];
    controller.update();
  }
}

class ColumnWidget extends ElementWidget {
  Widget layoutHandle = const Icon(Icons.arrow_downward);
  ColumnWidget(
      {super.key,
      required super.workController,
      widgetParams,
      super.parentElementController}) {
    elementController.type = 'column';
    elementController.layoutType = 'children';
    elementController.widgetParams = widgetParams;
  }
  @override
  Widget build(BuildContext context) {
    if (elementController.workController.mode == 'design') {
      return ElementActionWidget(
          elementController: elementController,
          child: GestureDetector(
            onDoubleTapDown: (details) {
              doubletapDetails = details.localPosition;
            },
            onDoubleTap: () {
              Offset position = doubletapDetails;
              _displayMenu(
                  context, position, elementController, elementRegistry);
            },
            child: GetBuilder<ElementController>(
              init: elementController,
              global: false,
              builder: (_) {
                return Row(
                  children: [
                    layoutHandle,
                    Container(
                      decoration:
                          BoxDecoration(border: Border.all(color: Colors.red)),
                      child: Column(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment:
                              ThemeDecoder.decodeMainAxisAlignment(
                                      elementController.widgetParams
                                          .widgetArgs["mainAxisAlignment"]) ??
                                  MainAxisAlignment.start,
                          crossAxisAlignment:
                              ThemeDecoder.decodeCrossAxisAlignment(
                                      elementController.widgetParams
                                          .widgetArgs["crossAxisAlignment"]) ??
                                  CrossAxisAlignment.center,
                          children: [...elementController.children]),
                    )
                  ],
                );
              },
            ),
          ));
    } else {
      return Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: ThemeDecoder.decodeMainAxisAlignment(
                  elementController
                      .widgetParams.widgetArgs["mainAxisAlignment"]) ??
              MainAxisAlignment.start,
          crossAxisAlignment: ThemeDecoder.decodeCrossAxisAlignment(
                  elementController
                      .widgetParams.widgetArgs["crossAxisAlignment"]) ??
              CrossAxisAlignment.center,
          children: [...elementController.children]);
    }
  }
}

_displayMenu(BuildContext context, Offset position,
    ElementController elementController, JsonWidgetRegistry registry) {
  ColumnWidgetParams widgetParams = elementController.widgetParams;
  Get.defaultDialog(
    title: '设置column',
    titleStyle: const TextStyle(fontSize: 18),
    content: Padding(
      padding: const EdgeInsets.all(8.0),
      child: SizedBox(
        width: 380,
        height: 200,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            GetBuilder<ElementController>(
              init: elementController,
              global: false,
              builder: (_) {
                return ToggleWidget(
                  titleWidget: const Text("主轴对齐:"),
                  initialLabelIndex: widgetParams.getMainAxisAlignmentIndex(),
                  labels: widgetParams.mainAxisAlignmentLabelsMap.keys.toList(),
                  onToggle: (index) {
                    widgetParams.setMainAxisAlignment(
                        index!, elementController);
                  },
                );
              },
            ),
            GetBuilder<ElementController>(
              init: elementController,
              global: false,
              builder: (_) {
                return ToggleWidget(
                  titleWidget: const Text("辅轴对齐:"),
                  initialLabelIndex: widgetParams.getCrossAxisAlignmentIndex(),
                  labels:
                      widgetParams.crossAxisAlignmentLabelsMap.keys.toList(),
                  onToggle: (index) {
                    widgetParams.setCrossAxisAlignment(
                        index!, elementController);
                  },
                );
              },
            ),
          ],
        ),
      ),
    ),
  );
}
