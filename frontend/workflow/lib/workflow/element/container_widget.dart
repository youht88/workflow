import 'dart:math';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:json_dynamic_widget/json_dynamic_widget.dart';
import 'package:json_theme/json_theme.dart';
import 'package:workflow/workflow/element/element_controller.dart';
import 'package:workflow/workflow/tools/input_widget.dart';
import 'package:workflow/workflow/tools/multi_slider_widget.dart';
import 'package:workflow/workflow/tools/toggle_widget.dart';

import '../tools/color_picker_widget.dart';
import 'element_widget.dart';

class ContainerWidgetParams {
  Map<String, dynamic>? widgetJsonData;
  late Map<String, dynamic> widgetArgs;

  String? verticalAlignment = "center";
  String? horizontalAlignment = "center";
  late bool isTransparent;
  late bool isGradian;
  late bool noBorder;
  ContainerWidgetParams({widgetJsonData, widgetArgs}) {
    this.widgetArgs = widgetArgs ??
        {
          "width": null,
          "height": null,
          "padding": [0.0, 0.0, 0.0, 0.0],
          "color": null,
          "alignment": null,
          "imageUrl": null,
          "borderColor": null,
          "borderRadius": 0.0,
          "gradianColor": null
        };
    isTransparent = (ThemeDecoder.decodeColor(this.widgetArgs['color']) ??
            Colors.transparent) ==
        Colors.transparent;
    noBorder = (ThemeDecoder.decodeColor(this.widgetArgs['borderColor']) ??
            Colors.transparent) ==
        Colors.transparent;
    isGradian =
        ThemeDecoder.decodeColor(this.widgetArgs['gradianColor']) != null;
    Map<String, dynamic> jsonData = {
      "type": "container",
      "args": {
        "width": r"${width}",
        "height": r"${height}",
      },
      "child": {
        "type": "decorated_box",
        "args": {
          "decoration": {
            "border": {
              "bottom": {"color": "#ff0000ff", "width": 1.0},
            },
            "image": {
              "image": {"scale": 1.0, "type": "network", "url": r"${url}"},
              "fit": "cover"
            },
            "color": r"${color}",
            "shape": r"${shape}"
          }
        },
        "child": {
          "type": "align",
          "args": {"alignment": r"${alignment}"},
          "child": r"${child}"
        }
      }
    };
    this.widgetJsonData = widgetJsonData ?? jsonData;
  }
  setWidth(width, ElementController controller) {
    widgetArgs['width'] = (width == 0) ? null : width;
    controller.update();
  }

  setHeight(height, controller) {
    widgetArgs['height'] = (height == 0) ? null : height;
    controller.update();
  }

  setPadding(int index, double value, controller) {
    widgetArgs['padding'][index] = value.toInt().toDouble();
    controller.update();
  }

  setColor(Color? color, controller) {
    isTransparent = (color ?? Colors.transparent) == Colors.transparent;
    widgetArgs['color'] = ThemeEncoder.encodeColor(color);
    controller.update();
  }

  setGradianColor(Color? color, controller) {
    widgetArgs['gradianColor'] = ThemeEncoder.encodeColor(color);
    controller.update();
  }

  setBorderColor(Color? color, controller) {
    if (color != null) {
      noBorder = false;
      widgetArgs['borderColor'] = ThemeEncoder.encodeColor(color);
    } else {
      noBorder = true;
      widgetArgs['borderColor'] = null;
    }
    controller.update();
  }

  setBorderRadius(double? radius, controller) {
    if (radius != null) {
      widgetArgs['borderRadius'] = radius.toInt().toDouble();
    } else {
      widgetArgs['borderRadius'] = 0.0;
    }
    controller.update();
  }

  double getMaxBorderRadius() {
    double width = widgetArgs['width'] ?? 0.0;
    double height = widgetArgs['height'] ?? 0.0;
    double maxBorderRadius = min(width, height) / 2;
    return maxBorderRadius == 0 ? 20 : maxBorderRadius;
  }

  Map<String, String> horizontalAlignmentLabelsMap = {
    "居左": "left",
    "居中": "center",
    "居右": "right",
  };
  Map<String, String> verticalAlignmentLabelsMap = {
    "居上": "top",
    "居中": "center",
    "居下": "bottom",
  };
  Map<String, AlignmentGeometry> alignmentLabelsMap = {
    "居上居左": Alignment.topLeft,
    "居上居中": Alignment.topCenter,
    "居上居右": Alignment.topRight,
    "居中居左": Alignment.centerLeft,
    "居中居中": Alignment.center,
    "居中居右": Alignment.centerRight,
    "居下居左": Alignment.bottomLeft,
    "居下居中": Alignment.bottomCenter,
    "居下居右": Alignment.bottomRight,
  };
  int getVerticalAlignmentIndex() {
    return verticalAlignmentLabelsMap.values
        .toList()
        .indexOf(verticalAlignment ?? "center");
  }

  setVerticalAlignment(int? index, ElementController controller) {
    verticalAlignment = verticalAlignmentLabelsMap.values.toList()[index ?? 1];
    setAlignment();
    controller.update();
  }

  int getHorizontalAlignmentIndex() {
    return horizontalAlignmentLabelsMap.values
        .toList()
        .indexOf(horizontalAlignment ?? "center");
  }

  setHorizontalAlignment(int? index, ElementController controller) {
    horizontalAlignment =
        horizontalAlignmentLabelsMap.values.toList()[index ?? 1];
    setAlignment();
    controller.update();
  }

  setAlignment() {
    int hIndex = getHorizontalAlignmentIndex();
    int vIndex = getVerticalAlignmentIndex();
    debugPrint("$hIndex,$vIndex--${alignmentLabelsMap.values.toList()}");
    widgetArgs['alignment'] = ThemeEncoder.encodeAlignmentGeometry(
        alignmentLabelsMap.values.toList()[vIndex * 3 + hIndex]);
  }
}

class ContainerWidget extends ElementWidget {
  ContainerWidgetParams widgetParams;
  Widget layoutHandle = const Icon(Icons.add_box_outlined);
  ContainerWidget(
      {super.key,
      required super.workController,
      required this.widgetParams,
      super.parentElementController}) {
    elementController.type = 'container';
    elementController.layoutType = 'child';
    elementController.widgetParams = widgetParams;
  }
  @override
  Widget build(BuildContext context) {
    Widget main = JsonWidgetData.fromDynamic(
            elementController.widgetParams.widgetJsonData,
            registry: elementRegistry)!
        .build(context: context);

    return elementController.workController.mode == 'design'
        ? ElementActionWidget(
            elementController: elementController,
            child: GestureDetector(
                onDoubleTapDown: (details) {
                  doubletapDetails = details.localPosition;
                },
                onDoubleTap: () {
                  Offset position = doubletapDetails;
                  _displayMenu(
                      context, position, elementController, elementRegistry);
                },
                //child: elementController.child!,
                child: Container(
                  decoration:
                      BoxDecoration(border: Border.all(color: Colors.black12)),
                  child: GetBuilder<ElementController>(
                    init: elementController,
                    global: false,
                    builder: (_) {
                      return Row(children: [
                        layoutHandle,
                        Container(
                          width: elementController
                              .widgetParams.widgetArgs['width'],
                          height: elementController
                              .widgetParams.widgetArgs['height'],
                          padding: EdgeInsets.only(
                              left: elementController
                                      .widgetParams.widgetArgs['padding']?[0]
                                      .toDouble() ??
                                  0.0,
                              top: elementController
                                      .widgetParams.widgetArgs['padding']?[1]
                                      .toDouble() ??
                                  0.0,
                              right: elementController
                                      .widgetParams.widgetArgs['padding']?[2]
                                      .toDouble() ??
                                  0.0,
                              bottom: elementController
                                      .widgetParams.widgetArgs['padding']?[3]
                                      .toDouble() ??
                                  0.0),
                          decoration: BoxDecoration(
                              border: !widgetParams.noBorder
                                  ? Border.all(
                                      color: ThemeDecoder.decodeColor(widgetParams
                                          .widgetArgs['borderColor'])!)
                                  : null,
                              borderRadius: (widgetParams.widgetArgs['borderRadius'] ?? 0.0) != 0.0
                                  ? BorderRadius.circular(
                                      widgetParams.widgetArgs['borderRadius'])
                                  : null,
                              gradient: widgetParams.isGradian
                                  ? LinearGradient(colors: [
                                      ThemeDecoder.decodeColor(elementController
                                              .widgetParams
                                              .widgetArgs['color']) ??
                                          Colors.transparent,
                                      ThemeDecoder.decodeColor(elementController
                                              .widgetParams
                                              .widgetArgs['gradianColor']) ??
                                          Colors.transparent,
                                    ])
                                  : null,
                              image: widgetParams.widgetArgs['imageUrl'] != null
                                  ? DecorationImage(
                                      image: NetworkImage(
                                          widgetParams.widgetArgs['imageUrl']),
                                      fit: BoxFit.cover)
                                  : null,
                              color: !widgetParams.isGradian
                                  ? ThemeDecoder.decodeColor(elementController
                                      .widgetParams.widgetArgs['color'])
                                  : null),
                          child: Align(
                              alignment: ThemeDecoder.decodeAlignment(
                                      elementController.widgetParams
                                          .widgetArgs['alignment']) ??
                                  Alignment.center,
                              child: elementController.child),
                        ),
                      ]);
                    },
                  ),
                )),
          )
        : Container(
            width: elementController.widgetParams.widgetArgs['width'],
            height: elementController.widgetParams.widgetArgs['height'],
            padding: EdgeInsets.only(
              left: elementController.widgetParams.widgetArgs['padding'][0]
                  .toDouble(),
              top: elementController.widgetParams.widgetArgs['padding'][1]
                  .toDouble(),
              right: elementController.widgetParams.widgetArgs['padding'][2]
                  .toDouble(),
              bottom: elementController.widgetParams.widgetArgs['padding'][3]
                  .toDouble(),
            ),
            decoration: BoxDecoration(
                image: widgetParams.widgetArgs['imageUrl'] != null
                    ? DecorationImage(
                        image:
                            NetworkImage(widgetParams.widgetArgs['imageUrl']),
                        fit: BoxFit.cover)
                    : null,
                border: widgetParams.widgetArgs['borderColor'] != null
                    ? Border.all(
                        color: ThemeDecoder.decodeColor(
                            widgetParams.widgetArgs['borderColor'])!)
                    : null,
                gradient: widgetParams.isGradian
                    ? LinearGradient(colors: [
                        ThemeDecoder.decodeColor(elementController
                                .widgetParams.widgetArgs['color']) ??
                            Colors.transparent,
                        ThemeDecoder.decodeColor(elementController
                                .widgetParams.widgetArgs['gradianColor']) ??
                            Colors.transparent,
                      ])
                    : null,
                borderRadius:
                    (widgetParams.widgetArgs['borderRadius'] ?? 0.0) != 0.0
                        ? BorderRadius.circular(
                            widgetParams.widgetArgs['borderRadius'])
                        : null,
                color: !widgetParams.isGradian
                    ? ThemeDecoder.decodeColor(
                        elementController.widgetParams.widgetArgs['color'])
                    : null),
            child: Align(
                alignment: ThemeDecoder.decodeAlignment(elementController
                        .widgetParams.widgetArgs['alignment']) ??
                    Alignment.center,
                child: elementController.child),
          );
  }
}

_displayMenu(BuildContext context, Offset position,
    ElementController elementController, JsonWidgetRegistry registry) {
  ContainerWidgetParams widgetParams = elementController.widgetParams;
  Get.defaultDialog(
    //onConfirm: () => print("Ok"),
    title: "设置container",
    titleStyle: const TextStyle(fontSize: 18),
    content: Padding(
      padding: const EdgeInsets.all(8.0),
      child: SizedBox(
        width: 250,
        height: 350,
        child: SingleChildScrollView(
          child: Column(
            children: [
              GetBuilder(
                  init: elementController,
                  global: false,
                  builder: (_) {
                    return ColorPickerWidget(
                      initColor: ThemeDecoder.decodeColor(elementController
                              .widgetParams.widgetArgs['color']) ??
                          Colors.transparent,
                      //width: 220,
                      //height: 120,
                      titleWidget: Row(
                        children: [
                          const Text("背景颜色"),
                          const SizedBox(width: 10),
                          Row(
                            children: [
                              Checkbox(
                                  value: widgetParams.isTransparent,
                                  onChanged: (value) {
                                    widgetParams.isTransparent =
                                        !widgetParams.isTransparent;
                                    if (value!) {
                                      widgetParams.setColor(Colors.transparent,
                                          elementController);
                                    }
                                  }),
                              const Text("透明"),
                            ],
                          ),
                          Row(
                            children: [
                              Checkbox(
                                  value: widgetParams.isGradian,
                                  onChanged: (value) {
                                    if (value!) {
                                      widgetParams.isGradian = true;
                                    } else {
                                      widgetParams.isGradian = false;
                                      elementController.elementRegistry
                                          .removeValue("gradianColor");
                                    }
                                    elementController.update();
                                  }),
                              const Text("渐变"),
                            ],
                          )
                        ],
                      ),
                      onColorChange: (Color color) {
                        widgetParams.setColor(color, elementController);
                      },
                    );
                  }),
              GetBuilder(
                  init: elementController,
                  global: false,
                  builder: (_) {
                    return widgetParams.isGradian
                        ? ColorPickerWidget(
                            initColor: ThemeDecoder.decodeColor(
                                    elementController.widgetParams
                                        .widgetArgs['gradianColor']) ??
                                Colors.transparent,
                            //width: 220,
                            //height: 120,
                            titleWidget: const Text("渐变到:"),
                            onColorChange: (Color color) {
                              widgetParams.setGradianColor(
                                  color, elementController);
                            },
                          )
                        : const SizedBox();
                  }),
              GetBuilder<ElementController>(
                init: elementController,
                global: false,
                builder: (_) {
                  return MultiSliderWidget(
                    height: 120,
                    titleWidget: Text(
                        "尺寸: ${widgetParams.widgetArgs['width']?.toInt()}  ✖️  ${widgetParams.widgetArgs['height']?.toInt()}"),
                    labels: const ['宽度:', '高度:'],
                    labelsTextStyle: const [
                      TextStyle(fontSize: 12, color: Colors.black45),
                      TextStyle(fontSize: 12, color: Colors.black45)
                    ],
                    sliderMin: const [0, 0],
                    sliderMax: [
                      elementController.workController.width - 80,
                      elementController.workController.height - 80
                    ],
                    sliderValue: [
                      widgetParams.widgetArgs['width'] ?? 0,
                      widgetParams.widgetArgs['height'] ?? 0
                    ],
                    sliderOnChanged: [
                      (value) {
                        widgetParams.setWidth(value, elementController);
                      },
                      (value) {
                        widgetParams.setHeight(value, elementController);
                      }
                    ],
                  );
                },
              ),
              ToggleWidget(
                  initialLabelIndex: widgetParams.getHorizontalAlignmentIndex(),
                  titleWidget:
                      const Text("水平对齐方式:", style: TextStyle(fontSize: 14)),
                  labels:
                      widgetParams.horizontalAlignmentLabelsMap.keys.toList(),
                  icons: const [
                    Icons.align_horizontal_left,
                    Icons.align_horizontal_center,
                    Icons.align_horizontal_right,
                  ],
                  onToggle: (index) {
                    widgetParams.setHorizontalAlignment(
                        index, elementController);
                  }),
              ToggleWidget(
                  initialLabelIndex: widgetParams.getVerticalAlignmentIndex(),
                  titleWidget:
                      const Text("垂直对齐方式:", style: TextStyle(fontSize: 14)),
                  labels: widgetParams.verticalAlignmentLabelsMap.keys.toList(),
                  icons: const [
                    Icons.align_horizontal_left,
                    Icons.align_horizontal_center,
                    Icons.align_horizontal_right,
                  ],
                  onToggle: (index) {
                    widgetParams.setVerticalAlignment(index, elementController);
                  }),
              GetBuilder(
                  init: elementController,
                  global: false,
                  builder: (_) {
                    return MultiSliderWidget(
                        height: 240,
                        titleWidget: Text(
                            "内边距: ${elementController.widgetParams.widgetArgs['padding'].map((item) => item.toInt()).toList()}"),
                        labels: const [
                          '左边:',
                          '上边:',
                          "右边:",
                          "下边:"
                        ],
                        labelsTextStyle: const [
                          TextStyle(fontSize: 12, color: Colors.black45),
                          TextStyle(fontSize: 12, color: Colors.black45),
                          TextStyle(fontSize: 12, color: Colors.black45),
                          TextStyle(fontSize: 12, color: Colors.black45)
                        ],
                        sliderMin: const [
                          0,
                          0,
                          0,
                          0
                        ],
                        sliderMax: const [
                          25,
                          25,
                          25,
                          25
                        ],
                        sliderValue: [
                          elementController
                              .widgetParams.widgetArgs['padding'][0]
                              .toDouble(),
                          elementController
                              .widgetParams.widgetArgs['padding'][1]
                              .toDouble(),
                          elementController
                              .widgetParams.widgetArgs['padding'][2]
                              .toDouble(),
                          elementController
                              .widgetParams.widgetArgs['padding'][3]
                              .toDouble(),
                        ],
                        sliderOnChanged: [
                          (value) => (elementController.widgetParams
                                  as ContainerWidgetParams)
                              .setPadding(0, value, elementController),
                          (value) => (elementController.widgetParams
                                  as ContainerWidgetParams)
                              .setPadding(1, value, elementController),
                          (value) => (elementController.widgetParams
                                  as ContainerWidgetParams)
                              .setPadding(2, value, elementController),
                          (value) => (elementController.widgetParams
                                  as ContainerWidgetParams)
                              .setPadding(3, value, elementController)
                        ]);
                  }),
              InputWidget(
                titleText: "背景图片地址:",
                initText: widgetParams.widgetArgs["imageUrl"],
                hintText: "输入URL地址",
                onSubmitted: (value) =>
                    widgetParams.widgetArgs["imageUrl"] = value,
              ),
              const Divider(),
              GetBuilder(
                  init: elementController,
                  global: false,
                  builder: (context) {
                    return ColorPickerWidget(
                      initColor: ThemeDecoder.decodeColor(elementController
                              .widgetParams.widgetArgs['borderColor']) ??
                          Colors.transparent,
                      //width: 220,
                      //height: 120,
                      titleWidget: Row(
                        children: [
                          const Text("边框颜色"),
                          const SizedBox(width: 10),
                          Row(
                            children: [
                              Checkbox(
                                  value: widgetParams.noBorder,
                                  onChanged: (value) {
                                    if (value!) {
                                      widgetParams.setBorderColor(
                                          null, elementController);
                                    } else {
                                      widgetParams.setBorderColor(
                                          Colors.black, elementController);
                                    }
                                  }),
                              const Text("无边框"),
                            ],
                          ),
                        ],
                      ),
                      onColorChange: (Color color) {
                        widgetParams.setBorderColor(color, elementController);
                      },
                    );
                  }),
              GetBuilder(
                  init: elementController,
                  global: false,
                  builder: (context) {
                    return MultiSliderWidget(
                      titleWidget: Text(
                          "设置圆角 ${widgetParams.widgetArgs['borderRadius'].toInt()}"),
                      labels: const ['值:'],
                      sliderValue: [widgetParams.widgetArgs['borderRadius']],
                      sliderMin: const [0],
                      sliderMax: [widgetParams.getMaxBorderRadius()],
                      sliderOnChanged: [
                        (value) => widgetParams.setBorderRadius(
                            value, elementController)
                      ],
                    );
                  }),
            ],
          ),
        ),
      ),
    ),
  );
}
