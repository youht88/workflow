import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:json_dynamic_widget/json_dynamic_widget.dart';
import 'package:json_theme/json_theme.dart';
import 'package:star_menu/star_menu.dart';
import 'package:workflow/workflow/menu/menu_params.dart';
import 'package:workflow/workflow/tools/toggle_widget.dart';

import 'element_controller.dart';
import 'element_widget.dart';

class RowWidgetParams {
  Map<String, dynamic>? widgetJsonData;
  late Map<String, dynamic> widgetArgs;
  Map<String, String> mainAxisAlignmentLabelsMap = {
    "居左": "start",
    "居中": "center",
    "居右": "end",
    "两端": "spaceBetween",
    "均分": "spaceAround",
  };
  Map<String, String> crossAxisAlignmentLabelsMap = {
    "居中": "center",
    "居上": "start",
    "居下": "end",
  };
  RowWidgetParams({widgetArgs}) {
    this.widgetArgs = widgetArgs ??
        {"mainAxisAlignment": "start", "crossAxisAlignment": "center"};
  }
  int getMainAxisAlignmentIndex() {
    return mainAxisAlignmentLabelsMap.values
        .toList()
        .indexOf(widgetArgs["mainAxisAlignment"] ?? "start");
  }

  setMainAxisAlignment(int? index, controller) {
    widgetArgs["mainAxisAlignment"] =
        mainAxisAlignmentLabelsMap.values.toList()[index ?? 0];
    controller.update();
  }

  int getCrossAxisAlignmentIndex() {
    return crossAxisAlignmentLabelsMap.values
        .toList()
        .indexOf(widgetArgs["crossAxisAlignment"] ?? CrossAxisAlignment.center);
  }

  setCrossAxisAlignment(int index, controller) {
    List test = crossAxisAlignmentLabelsMap.values.toList();
    widgetArgs["crossAxisAlignment"] = test[index];
    controller.update();
  }
}

class RowWidget extends ElementWidget {
  RowWidgetParams widgetParams;
  Widget layoutHandle = const Icon(Icons.arrow_forward);
  RowWidget({
    super.key,
    required super.workController,
    required this.widgetParams,
    super.parentElementController,
  }) {
    elementController.type = 'row';
    elementController.layoutType = "children";
    elementController.widgetParams = widgetParams;
  }
  @override
  Widget build(BuildContext context) {
    if (elementController.workController.mode == 'design') {
      return ElementActionWidget(
          elementController: elementController,
          child: GestureDetector(
            onDoubleTapDown: (details) {
              doubletapDetails = details.localPosition;
            },
            onDoubleTap: () {
              Offset position = doubletapDetails;
              _displayMenu(
                  context, position, elementController, elementRegistry);
            },
            child: GetBuilder<ElementController>(
              init: elementController,
              global: false,
              builder: (_) {
                return Row(
                  children: [
                    layoutHandle,
                    Container(
                      decoration:
                          BoxDecoration(border: Border.all(color: Colors.red)),
                      child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment:
                              ThemeDecoder.decodeMainAxisAlignment(
                                      elementController.widgetParams
                                          .widgetArgs["mainAxisAlignment"]) ??
                                  MainAxisAlignment.start,
                          crossAxisAlignment:
                              ThemeDecoder.decodeCrossAxisAlignment(
                                      elementController.widgetParams
                                          .widgetArgs["crossAxisAlignment"]) ??
                                  CrossAxisAlignment.center,
                          children: [...elementController.children]),
                    )
                  ],
                );
              },
            ),
          ));
    } else {
      return Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: ThemeDecoder.decodeMainAxisAlignment(
                  elementController
                      .widgetParams.widgetArgs["mainAxisAlignment"]) ??
              MainAxisAlignment.start,
          crossAxisAlignment: ThemeDecoder.decodeCrossAxisAlignment(
                  elementController
                      .widgetParams.widgetArgs["crossAxisAlignment"]) ??
              CrossAxisAlignment.center,
          children: [...elementController.children]);
    }
  }
}

_displayMenu(BuildContext context, Offset position,
    ElementController elementController, JsonWidgetRegistry registry) {
  RowWidgetParams widgetParams = elementController.widgetParams;
  Get.defaultDialog(
    title: '设置row',
    titleStyle: const TextStyle(fontSize: 18),
    content: Padding(
      padding: const EdgeInsets.all(8.0),
      child: SizedBox(
        width: 380,
        height: 200,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            GetBuilder<ElementController>(
              init: elementController,
              global: false,
              builder: (_) {
                return ToggleWidget(
                  titleWidget: const Text("主轴对齐:"),
                  initialLabelIndex: widgetParams.getMainAxisAlignmentIndex(),
                  labels: widgetParams.mainAxisAlignmentLabelsMap.keys.toList(),
                  onToggle: (index) {
                    widgetParams.setMainAxisAlignment(
                        index!, elementController);
                  },
                );
              },
            ),
            GetBuilder<ElementController>(
              init: elementController,
              global: false,
              builder: (_) {
                return ToggleWidget(
                  titleWidget: const Text("辅轴对齐:"),
                  initialLabelIndex: widgetParams.getCrossAxisAlignmentIndex(),
                  labels:
                      widgetParams.crossAxisAlignmentLabelsMap.keys.toList(),
                  onToggle: (index) {
                    widgetParams.setCrossAxisAlignment(
                        index!, elementController);
                  },
                );
              },
            ),
          ],
        ),
      ),
    ),
  );
}
