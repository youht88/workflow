import 'package:flex_color_picker/flex_color_picker.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:json_dynamic_widget/json_dynamic_widget.dart';
import 'package:star_menu/star_menu.dart';
import 'package:workflow/util/data_util/icon_lib.dart';
import 'package:workflow/workflow/element/element_controller.dart';
import 'package:workflow/workflow/menu/menu_params.dart';
import 'package:workflow/workflow/tools/icon_picker_widget.dart';
import 'package:workflow/workflow/tools/input_widget.dart';
import 'package:workflow/workflow/tools/toggle_widget.dart';

import 'element_widget.dart';

class IconWidgetParams {
  late Map<String, dynamic> widgetJsonData;
  String? codePoint;
  num? size = 20;
  IconWidgetParams({widgetJsonData, this.codePoint = "57399"}) {
    Map<String, dynamic> jsonData = {
      "type": "icon",
      "args": {
        "icon": {"codePoint": r"${codePoint}", "fontFamily": "MaterialIcons"},
        "size": r"${size}",
        "color": r"${color}"
      }
    };
    this.widgetJsonData = widgetJsonData ?? jsonData;
  }

  Map<String, num> fontSizeLabelsMap = {
    "小": 14.0,
    "中": 20.0,
    "大": 28.0,
  };
  int getFontSizeIndex() {
    return fontSizeLabelsMap.values.toList().indexOf(size ?? 20);
  }

  setFontSize(int? index, ElementController controller) {
    size = fontSizeLabelsMap.values.toList()[index ?? 1];
    controller.elementRegistry
        .setValue("size", fontSizeLabelsMap.values.toList()[index ?? 1]);
    controller.update();
  }
}

class IconWidget extends ElementWidget {
  IconWidget(
      {super.key,
      required super.workController,
      widgetParams,
      super.parentElementController}) {
    elementController.widgetParams = widgetParams;
    elementController.type = 'icon';
    elementRegistry.setValue("name", null);
    elementRegistry.setValue("valueKey", "codePoint");
    elementRegistry.setValue("codePoint", widgetParams.codePoint);
    elementRegistry.setValue("size", widgetParams.size);
  }
  @override
  Widget build(BuildContext context) {
    Widget child = JsonWidgetData.fromDynamic(
            elementController.widgetParams.widgetJsonData,
            registry: elementRegistry)!
        .build(context: context);

    return elementController.workController.mode == 'design'
        ? ElementActionWidget(
            elementController: elementController,
            child: GestureDetector(
                onDoubleTapDown: (details) {
                  doubletapDetails = details.localPosition;
                },
                onDoubleTap: () {
                  Offset position = doubletapDetails;
                  _displayMenu(
                      context, position, elementController, elementRegistry);
                },
                //child: elementController.child!,
                child: Container(
                    decoration:
                        BoxDecoration(border: Border.all(color: Colors.red)),
                    child: child)),
          )
        : child;
  }
}

_displayMenu(BuildContext context, Offset position,
    ElementController elementController, JsonWidgetRegistry registry) {
  IconsMap;
  Get.defaultDialog(
    title: '设置图标',
    titleStyle: const TextStyle(fontSize: 18),
    content: SizedBox(
      width: 240,
      height: 380,
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            InputWidget(
                initText: registry.getValue("name"),
                titleText: "输入名字:",
                hintText: "input name",
                onChanged: (value) {
                  if (value == "") value = "name";
                  registry.setValue("name", value);
                }),
            IconPickerWidget(
              onIconChange: (IconData value) {
                registry.setValue("codePoint", value.codePoint);
              },
              crossAxisCount: 6,
              height: 250,
            ),
            ColorPicker(
              //color: workController.bgColor,
              title: const Text("图标颜色:"),
              width: 15,
              height: 15,
              padding: const EdgeInsets.all(2),
              pickersEnabled: const <ColorPickerType, bool>{
                ColorPickerType.custom: true,
                ColorPickerType.primary: false,
                ColorPickerType.accent: false,
              },
              onColorChanged: (Color value) {
                registry.setValue("color", value.hex);
              },
            ),
            ToggleWidget(
              titleWidget: const Text("图标大小:", style: TextStyle(fontSize: 14)),
              initialLabelIndex:
                  elementController.widgetParams.getFontSizeIndex(),
              labels: elementController.widgetParams.fontSizeLabelsMap.keys
                  .toList(),
              labelsTextStyle: const [
                TextStyle(fontSize: 14),
                TextStyle(fontSize: 20),
                TextStyle(fontSize: 28)
              ],
              onToggle: (index) {
                elementController.widgetParams
                    .setFontSize(index, elementController);
              },
            ),
          ],
        ),
      ),
    ),
  );
}
