import 'package:flutter/material.dart';

class ChatSystemWidget extends StatelessWidget {
  Widget item;
  ChatSystemWidget({super.key, required this.item});

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.all(4.0),
        child: Center(
          child: Container(
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(5)),
                shape: BoxShape.rectangle,
                color: Colors.black12,
              ),
              child: Padding(
                padding: const EdgeInsets.all(4.0),
                child: item,
              )),
        ));
  }
}
