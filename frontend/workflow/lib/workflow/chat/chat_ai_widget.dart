import 'package:flutter/material.dart';

class ChatAiWidget extends StatelessWidget {
  Widget item;
  ChatAiWidget({super.key, required this.item});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(4.0),
      child: Container(
        child: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
          Container(
            height: 40,
            width: 40,
            decoration: const BoxDecoration(
                shape: BoxShape.circle, color: Colors.purple),
            child: const Icon(Icons.school, color: Colors.white70),
          ),
          const SizedBox(width: 8),
          Expanded(
            child: Container(
              //color: Colors.white,
              child: SingleChildScrollView(
                  //scrollDirection: Axis.horizontal,
                  child: Padding(
                padding: const EdgeInsets.all(4.0),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Container(
                      decoration: BoxDecoration(
                        borderRadius:
                            const BorderRadius.all(Radius.circular(5)),
                        shape: BoxShape.rectangle,
                        color: Colors.purple.shade50,
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(4.0),
                        child: item,
                      )),
                ),
              )),
            ),
          ),
        ]),
      ),
    );
  }
}
