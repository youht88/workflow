import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:workflow/workflow/chat/chat_controller.dart';

class ChatWidget extends StatelessWidget {
  late ChatController controller;
  ChatWidget({super.key, required projectController}) {
    Get.put(ChatController());
    controller = Get.find<ChatController>();
    controller.widget = this;
    controller.projectController = projectController;
    controller.postInit();
  }

  @override
  Widget build(BuildContext context) {
    debugPrint("${controller.chats}");
    return Scaffold(
        appBar: AppBar(title: Text(controller.projectController.projectName)),
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              Expanded(
                  child: GetBuilder(
                      init: controller,
                      builder: (_) {
                        return ListView.builder(
                            reverse: true,
                            itemCount: controller.chats.length,
                            itemBuilder: (context, idx) {
                              return controller.chats[idx];
                            });
                      })),
              const SizedBox(height: 8),
              Row(
                children: [
                  Expanded(
                    child: TextField(
                        controller: controller.editController,
                        decoration: const InputDecoration(
                            border: OutlineInputBorder(),
                            contentPadding: EdgeInsets.all(10)),
                        minLines: 1,
                        maxLines: 4,
                        style: const TextStyle(fontSize: 14)),
                  ),
                  IconButton(
                      onPressed: () {
                        controller.addEditToChat(context);
                      },
                      icon: const Icon(Icons.send, color: Colors.blue))
                ],
              )
            ],
          ),
        ));
  }
}
