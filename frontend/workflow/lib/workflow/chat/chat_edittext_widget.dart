import 'package:flutter/material.dart';

class ChatUserWidget extends StatelessWidget {
  Widget item;
  ChatUserWidget({super.key, required this.item});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(4.0),
      child: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
        Expanded(
          child: Align(
            alignment: Alignment.centerRight,
            child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    shape: BoxShape.rectangle,
                    color: Colors.blue.shade50),
                child: Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: item,
                )),
          ),
        ),
        const SizedBox(width: 8),
        Container(
          height: 40,
          width: 40,
          decoration:
              const BoxDecoration(shape: BoxShape.circle, color: Colors.blue),
          child: const Icon(Icons.tag_faces, color: Colors.white70),
        )
      ]),
    );
  }
}
