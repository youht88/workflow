import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:get/get.dart';
import 'package:workflow/util/network/http_util.dart';
import 'package:workflow/workflow/chat/chat_ai_widget.dart';
import 'package:workflow/workflow/chat/chat_edittext_widget.dart';
import 'package:workflow/workflow/chat/chat_page.dart';
import 'package:workflow/workflow/chat/chat_system_widget.dart';
import 'package:workflow/workflow/event/chat_system_event.dart';
import 'package:workflow/workflow/project/project_controller.dart';
import 'package:workflow/workflow/work/work_controller.dart';

class ChatController extends GetxController {
  late ChatWidget widget;
  late ProjectController projectController;
  List<Widget> chats = [];
  TextEditingController editController = TextEditingController();

  postInit() {
    projectController.eventBus.on<ChatSystemEvent>().listen((event) {
      if (event.widgetOrText is Widget) {
        addSystemChat(event.widgetOrText);
      } else {
        addSystemChat(SelectableText("${event.widgetOrText}"));
      }
    });
  }

  addEditToChat(BuildContext context) {
    String text = editController.text;
    chats.insert(0, ChatUserWidget(item: Text(text)));
    editController.text = "";
    update();
    magicMessage(context, text);
  }

  addAiChat(Widget item) {
    chats.insert(0, ChatAiWidget(item: item));
    update();
  }

  addSystemChat(Widget item) {
    chats.insert(0, ChatSystemWidget(item: item));
    update();
  }

  magicMessage(BuildContext context, String text) async {
    switch (text) {
      case "start":
        start(context, text);
        break;
      case "close":
        Get.back();
      case "system":
        projectController.eventBus.fire(ChatSystemEvent("hello"));
      default:
        final message = await sendToLLM(text);
        //Widget item = const Text("hello youhaitao!");
        addAiChat(MarkdownBody(
          data: message,
          selectable: true,
          //syntaxHighlighter: getHighlighter(),
        ));
    }
  }

  Future<String> sendToLLM(String text) async {
    final message = await HttpLib.getData(
        "http://openai.gpt4.vip:9322/openai/chat?prompt=$text",
        responseType: ResponseType.plain);
    return message;
  }

  start(BuildContext context, String text) async {
    if (projectController.works.isEmpty) return;
    WorkController workController = projectController.works[0].workController;
    if (workController.isAiMode || workController.inputElements.isEmpty) {
      addAiChat(Text(workController.helloMessage));
      return;
    }
    Map<String, Widget> workPage = workController.generateWorkPage(context);
    String? outString;

    if (GetPlatform.isMobile) {
      outString = await Get.bottomSheet(
        backgroundColor: Colors.white,
        workPage['inputPage']!,
      );
    } else {
      outString = await Get.defaultDialog(
        title: "",
        content: workPage['inputPage']!,
      );
    }
    debugPrint(outString);
    if ((outString ?? "") != "") {
      if (projectController.works[0].workController.outputElements.isNotEmpty) {
        addAiChat(workPage['outputPage']!);
      } else {
        addAiChat(Text(outString!));
      }
    }
  }
}
